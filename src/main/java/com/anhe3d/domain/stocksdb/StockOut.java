package com.anhe3d.domain.stocksdb;

import com.anhe3d.domain.purchasedb.PurchaseOrder;
import com.anhe3d.domain.testdb.Employee;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(schema = "stocksdb", name = "STOCK_OUT")
public class StockOut {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "STOCK_OUT_ID")
    private Long stockOutId;

    @Column(name = "CREATE_TIME")
    private Timestamp createTime;

    @ManyToOne
    @JoinColumn(name = "EMPLOYEE_ID" , nullable = false)
    private Employee employee;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID" , nullable = false)
    private Item item;

    @ManyToOne
    @JoinColumn(name = "PO_ID" , nullable = false)
    private PurchaseOrder purchaseOrder;

    @Column(name = "QUANTITY")
    private int quantity;

    @Column(name = "REMARK")
    private String remark;

    @Column(name = "TIME")
    private Timestamp Time;



    public StockOut(){}

    public StockOut(Timestamp createTime, Employee employee, Item item, PurchaseOrder purchaseOrder, int quantity, Timestamp time) {
        this.createTime = createTime;
        this.employee = employee;
        this.item = item;
        this.purchaseOrder = purchaseOrder;
        this.quantity = quantity;
        Time = time;
    }

    public Long getStockOutId() {
        return stockOutId;
    }

    public void setStockOutId(Long stockOutId) {
        this.stockOutId = stockOutId;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Timestamp getTime() {
        return Time;
    }

    public void setTime(Timestamp time) {
        Time = time;
    }

    @Override
    public String toString() {
        return "StockOut{" +
                "stockOutId=" + stockOutId +
                ", createTime=" + createTime +
                ", employee=" + employee +
                ", item=" + item +
                ", purchaseOrder=" + purchaseOrder +
                ", quantity=" + quantity +
                ", remark='" + remark + '\'' +
                ", Time=" + Time +
                '}';
    }
}
