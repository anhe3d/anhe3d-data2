package com.anhe3d.domain.stocksdb;

import com.anhe3d.domain.purchasedb.PurchaseOrder;

import javax.persistence.*;

@Entity
@Table(schema = "stocksdb", name = "BATCH")
public class Batch {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "BATCH_ID")
    private Long batchId;


    @ManyToOne
    @JoinColumn(name = "PO_ID")
    private PurchaseOrder purchaseOrder;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID" , nullable = false)
    private Item item;

    @Column(name = "QUANTITY")
    private int quantity;

    public Batch(){}

    public Batch(PurchaseOrder purchaseOrder, Item item, int quantity) {
        this.purchaseOrder = purchaseOrder;
        this.item = item;
        this.quantity = quantity;
    }

    public Long getBatchId() {
        return batchId;
    }

    public void setBatchId(Long batchId) {
        this.batchId = batchId;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Batch{" +
                "batchId=" + batchId +
                ", purchaseOrder=" + purchaseOrder +
                ", item=" + item +
                ", quantity=" + quantity +
                '}';
    }
}
