package com.anhe3d.domain.stocksdb;

import javax.persistence.*;

@Entity
@Table(schema = "stocksdb", name = "STOCK")
public class Stock {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "STOCK_ID")
    private Long stockId;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID" , nullable = false)
    private Item item;

    @Column(name = "STORAGE_PLACE")
    private String storagePlace;

    @Column(name = "WAREHOUSE")
    private String warehouse;

    @Column(name = "QUANTITY")
    private int quantity;

    @Column(name = "SAFETY_STOCKS")
    private int safetyStocks;

    public Stock(){}

    public Stock(Item item, String storagePlace, String warehouse, int quantity, int safetyStocks) {
        this.item = item;
        this.storagePlace = storagePlace;
        this.warehouse = warehouse;
        this.quantity = quantity;
        this.safetyStocks = safetyStocks;
    }

    public Long getStockId() {
        return stockId;
    }

    public void setStockId(Long stockId) {
        this.stockId = stockId;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public String getStoragePlace() {
        return storagePlace;
    }

    public void setStoragePlace(String storagePlace) {
        this.storagePlace = storagePlace;
    }

    public String getWarehouse() {
        return warehouse;
    }

    public void setWarehouse(String warehouse) {
        this.warehouse = warehouse;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int getSafetyStocks() {
        return safetyStocks;
    }

    public void setSafetyStocks(int safetyStocks) {
        this.safetyStocks = safetyStocks;
    }

    @Override
    public String toString() {
        return "Stock{" +
                "stockId=" + stockId +
                ", item=" + item +
                ", storagePlace='" + storagePlace + '\'' +
                ", warehouse='" + warehouse + '\'' +
                ", quantity=" + quantity +
                ", safetyStocks=" + safetyStocks +
                '}';
    }
}
