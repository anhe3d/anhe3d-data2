package com.anhe3d.domain.stocksdb;

import javax.persistence.*;

@Entity
@Table(schema = "stocksdb", name = "ITEM_TYPE")
public class ItemType {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ITEMTYPE_ID")
    private Long itemTypeId;

    @Column(name = "ITEMTYPE_NAME")
    private String itemTypeName;

    public ItemType(){}

    public ItemType(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    public Long getItemTypeId() {
        return itemTypeId;
    }

    public void setItemTypeId(Long itemTypeId) {
        this.itemTypeId = itemTypeId;
    }

    public String getItemTypeName() {
        return itemTypeName;
    }

    public void setItemTypeName(String itemTypeName) {
        this.itemTypeName = itemTypeName;
    }

    @Override
    public String toString() {
        return "ItemType{" +
                "itemTypeId=" + itemTypeId +
                ", itemTypeName='" + itemTypeName + '\'' +
                '}';
    }
}
