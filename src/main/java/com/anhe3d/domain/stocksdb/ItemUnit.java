package com.anhe3d.domain.stocksdb;

import javax.persistence.*;

@Entity
@Table(schema = "stocksdb", name = "ITEM_UNIT")
public class ItemUnit {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "UNIT_ID")
    private Long unitId;

    @Column(name = "UNIT_NAME")
    private String unitName;

    public ItemUnit(){}

    public ItemUnit(String unitName) {
        this.unitName = unitName;
    }

    public Long getUnitId() {
        return unitId;
    }

    public void setUnitId(Long unitId) {
        this.unitId = unitId;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    @Override
    public String toString() {
        return "ItemUnit{" +
                "unitId=" + unitId +
                ", unitName='" + unitName + '\'' +
                '}';
    }
}
