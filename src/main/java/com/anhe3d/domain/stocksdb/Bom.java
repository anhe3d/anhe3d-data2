package com.anhe3d.domain.stocksdb;

import javax.persistence.*;

@Entity
@Table(schema = "stocksdb", name = "BOM")
public class Bom {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "BOM_ID")
    private Long bomId;

    @Column(name = "BOM_NAME")
    private String bomName;

    @ManyToOne
    @JoinColumn(name = "PARENT_BOM_ID" )
    private Bom bom;


    @ManyToOne
    @JoinColumn(name = "ITEM_ID" , nullable = false)
    private Item item;

    @Column(name = "QUANTITY")
    private int quantity;

    public Bom(){}

    public Bom(String bomName, Bom bom, Item item, int quantity) {
        this.bomName = bomName;
        this.bom = bom;
        this.item = item;
        this.quantity = quantity;
    }

    public Long getBomId() {
        return bomId;
    }

    public void setBomId(Long bomId) {
        this.bomId = bomId;
    }

    public String getBomName() {
        return bomName;
    }

    public void setBomName(String bomName) {
        this.bomName = bomName;
    }

    public Bom getBom() {
        return bom;
    }

    public void setBom(Bom bom) {
        this.bom = bom;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "Bom{" +
                "bomId=" + bomId +
                ", bomName='" + bomName + '\'' +
                ", bom=" + bom +
                ", item=" + item +
                ", quantity=" + quantity +
                '}';
    }
}
