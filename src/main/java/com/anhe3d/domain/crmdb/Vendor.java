package com.anhe3d.domain.crmdb;

import javax.persistence.*;

@Entity
@Table(schema = "crmdb", name = "VENDOR")
public class Vendor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "VENDOR_ID")
    private Long vendorId;

    @Column(name = "VENDOR_NAME")
    private String vendorName;

    @Column(name = "VAT_NUM")
    private String vatNum;

    @Column(name = "VENDOR_TEL")
    private String vendorTel;

    @Column(name = "VENDOR_FAX")
    private String vendorFax;

    @Column(name = "VENDOR_ADDR")
    private String vendorAddr;

    @Column(name = "VENDOR_WEB")
    private String vendorWeb;

    @Column(name = "CONTACT_NAME")
    private String contactName;

    @Column(name = "CONTACT_POSITION")
    private String contactPosition;

    @Column(name = "CONTACT_PHONE")
    private String contactPhone;

    @Column(name = "CONTACT_EMAIL")
    private String contactEmail;

    public Vendor() {
    }

    public Vendor(String vendorName, String vendorTel) {
        this.vendorName = vendorName;
        this.vendorTel = vendorTel;
    }

    public Long getVendorId() {
        return vendorId;
    }

    public void setVendorId(Long vendorId) {
        this.vendorId = vendorId;
    }

    public String getVendorName() {
        return vendorName;
    }

    public void setVendorName(String vendorName) {
        this.vendorName = vendorName;
    }

    public String getVatNum() {
        return vatNum;
    }

    public void setVatNum(String vatNum) {
        this.vatNum = vatNum;
    }

    public String getVendorTel() {
        return vendorTel;
    }

    public void setVendorTel(String vendorTel) {
        this.vendorTel = vendorTel;
    }

    public String getVendorFax() {
        return vendorFax;
    }

    public void setVendorFax(String vendorFax) {
        this.vendorFax = vendorFax;
    }

    public String getVendorAddr() {
        return vendorAddr;
    }

    public void setVendorAddr(String vendorAddr) {
        this.vendorAddr = vendorAddr;
    }

    public String getVendorWeb() {
        return vendorWeb;
    }

    public void setVendorWeb(String vendorWeb) {
        this.vendorWeb = vendorWeb;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPosition() {
        return contactPosition;
    }

    public void setContactPosition(String contactPosition) {
        this.contactPosition = contactPosition;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @Override
    public String toString() {
        return "Vendor{" +
                "vendorId=" + vendorId +
                ", vendorName='" + vendorName + '\'' +
                ", vatNum='" + vatNum + '\'' +
                ", vendorTel='" + vendorTel + '\'' +
                ", vendorFax='" + vendorFax + '\'' +
                ", vendorAddr='" + vendorAddr + '\'' +
                ", vendorWeb='" + vendorWeb + '\'' +
                ", contactName='" + contactName + '\'' +
                ", contactPosition='" + contactPosition + '\'' +
                ", contactPhone='" + contactPhone + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                '}';
    }
}
