package com.anhe3d.domain.crmdb;

import javax.persistence.*;

@Entity
@Table(schema = "crmdb", name = "DISTRIBUTOR")
public class Distributor {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "DT_ID")
    private Long dtId;

    @Column(name = "DT_NAME")
    private String dtName;

    @Column(name = "DT_VATNUM")
    private String dtVatnum;

    @Column(name = "DT_TEL")
    private String dtTel;

    @Column(name = "DT_FAX")
    private String dtFax;

    @Column(name = "DT_ADDR")
    private String dtAddr;

    @Column(name = "DT_WEB")
    private String dtWeb;

    @Column(name = "CONTACT_NAME")
    private String contactName;

    @Column(name = "CONTACT_POSITION")
    private String contactPosition;

    @Column(name = "CONTACT_PHONE")
    private String contactPhone;

    @Column(name = "CONTACT_EMAIL")
    private String contactEmail;

    public Distributor() {
    }

    public Distributor(String dtName, String dtTel) {
        this.dtName = dtName;
        this.dtTel = dtTel;
    }

    public Long getDtId() {
        return dtId;
    }

    public void setDtId(Long dtId) {
        this.dtId = dtId;
    }

    public String getDtName() {
        return dtName;
    }

    public void setDtName(String dtName) {
        this.dtName = dtName;
    }

    public String getDtVatnum() {
        return dtVatnum;
    }

    public void setDtVatnum(String dtVatnum) {
        this.dtVatnum = dtVatnum;
    }

    public String getDtTel() {
        return dtTel;
    }

    public void setDtTel(String dtTel) {
        this.dtTel = dtTel;
    }

    public String getDtFax() {
        return dtFax;
    }

    public void setDtFax(String dtFax) {
        this.dtFax = dtFax;
    }

    public String getDtAddr() {
        return dtAddr;
    }

    public void setDtAddr(String dtAddr) {
        this.dtAddr = dtAddr;
    }

    public String getDtWeb() {
        return dtWeb;
    }

    public void setDtWeb(String dtWeb) {
        this.dtWeb = dtWeb;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getContactPosition() {
        return contactPosition;
    }

    public void setContactPosition(String contactPosition) {
        this.contactPosition = contactPosition;
    }

    public String getContactPhone() {
        return contactPhone;
    }

    public void setContactPhone(String contactPhone) {
        this.contactPhone = contactPhone;
    }

    public String getContactEmail() {
        return contactEmail;
    }

    public void setContactEmail(String contactEmail) {
        this.contactEmail = contactEmail;
    }

    @Override
    public String toString() {
        return "Distributor{" +
                "dtId=" + dtId +
                ", dtName='" + dtName + '\'' +
                ", dtVatnum='" + dtVatnum + '\'' +
                ", dtTel='" + dtTel + '\'' +
                ", dtFax='" + dtFax + '\'' +
                ", dtAddr='" + dtAddr + '\'' +
                ", dtWeb='" + dtWeb + '\'' +
                ", contactName='" + contactName + '\'' +
                ", contactPosition='" + contactPosition + '\'' +
                ", contactPhone='" + contactPhone + '\'' +
                ", contactEmail='" + contactEmail + '\'' +
                '}';
    }
}
