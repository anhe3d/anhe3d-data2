package com.anhe3d.domain.salesdb;
import com.anhe3d.domain.stocksdb.Bom;
import com.anhe3d.domain.stocksdb.ItemType;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(schema = "salesdb", name = "SALES_ITEM")
public class SalesItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SITEM_ID")
    private Long sItemId;

    @Column(name = "SITEM_NAME")
    private String sItemName;

    @Column(name = "SITEM_PRICE")
    private Double sItemPrice;

    @Column(name = "SITEM_VENDORNUM")
    private String sItemVendorNum;

    @ManyToOne
    @JoinColumn(name = "ITEM_TYPE" , nullable = false)
    private ItemType itemType;

    @Column(name = "CREATE_TIME")
    private Timestamp createTime;

    @Column(name = "UPDATE_TIME")
    private Timestamp updateTime;

    @Column(name = "ENABLED")
    private Boolean enabled;

    @ManyToOne
    @JoinColumn(name = "BOM_ID" )
    private Bom bom;

    public SalesItem(){}

    public SalesItem(String sItemName, Double sItemPrice, ItemType itemType, String sItemVendorNum, Timestamp createTime, Timestamp updateTime, Boolean enabled, Bom bom) {
        this.sItemName = sItemName;
        this.sItemPrice = sItemPrice;
        this.itemType = itemType;
        this.sItemVendorNum = sItemVendorNum;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.enabled = enabled;
        this.bom = bom;
    }

    public Long getsItemId() {
        return sItemId;
    }

    public void setsItemId(Long sItemId) {
        this.sItemId = sItemId;
    }

    public String getsItemName() {
        return sItemName;
    }

    public void setsItemName(String sItemName) {
        this.sItemName = sItemName;
    }

    public Double getsItemPrice() {
        return sItemPrice;
    }

    public void setsItemPrice(Double sItemPrice) {
        this.sItemPrice = sItemPrice;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public String getsItemVendorNum() {
        return sItemVendorNum;
    }

    public void setsItemVendorNum(String sItemVendorNum) {
        this.sItemVendorNum = sItemVendorNum;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public Bom getBom() {
        return bom;
    }

    public void setBom(Bom bom) {
        this.bom = bom;
    }

    @Override
    public String toString() {
        return "SalesItem{" +
                "sItemId=" + sItemId +
                ", sItemName='" + sItemName + '\'' +
                ", sItemPrice=" + sItemPrice +
                ", itemType=" + itemType +
                ", sItemVendorNum='" + sItemVendorNum + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", enabled=" + enabled +
                ", bom=" + bom +
                '}';
    }
}
