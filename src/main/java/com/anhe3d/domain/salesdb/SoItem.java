package com.anhe3d.domain.salesdb;

import javax.persistence.*;

@Entity
@Table(schema = "salesdb", name = "SO_ITEM")
public class SoItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SOITEM_ID")
    private Long soItemId;

    @ManyToOne
    @JoinColumn(name = "SO_ID" , nullable = false)
    private SalesOrder salesOrder;

    @ManyToOne
    @JoinColumn(name = "SITEM_ID" , nullable = false)
//    @NotFound(action= NotFoundAction.IGNORE)
    private SalesItem salesItem;

    @Column(name = "QUANTITY")
    private int quantity;

    @Column(name = "PRICE")
    private double price;

    @Column(name = "SERIAL")
    private String serial;


    public SoItem(){}

    public SoItem(SalesOrder salesOrder, SalesItem salesItem, int quantity, double price, String serial) {
        this.salesOrder = salesOrder;
        this.salesItem = salesItem;
        this.quantity = quantity;
        this.price = price;
        this.serial = serial;
    }

    public Long getSoItemId() {
        return soItemId;
    }

    public void setSoItemId(Long soItemId) {
        this.soItemId = soItemId;
    }

    public SalesOrder getSalesOrder() {
        return salesOrder;
    }

    public void setSalesOrder(SalesOrder salesOrder) {
        this.salesOrder = salesOrder;
    }

    public SalesItem getSalesItem() {
        return salesItem;
    }

    public void setSalesItem(SalesItem salesItem) {
        this.salesItem = salesItem;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    @Override
    public String toString() {
        return "SoItem{" +
                "soItemId=" + soItemId +
                ", salesOrder=" + salesOrder +
                ", salesItem=" + salesItem +
                ", quantity=" + quantity +
                ", price=" + price +
                ", serial='" + serial + '\'' +
                '}';
    }
}
