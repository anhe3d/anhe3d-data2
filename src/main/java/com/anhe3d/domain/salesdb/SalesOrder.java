package com.anhe3d.domain.salesdb;
import com.anhe3d.domain.crmdb.Distributor;
import com.anhe3d.domain.testdb.Employee;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
@Table(schema = "salesdb", name = "SALES_ORDER")
public class SalesOrder {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SO_ID")
    private Long soId;

    @ManyToOne
    @JoinColumn(name = "DT_ID" , nullable = false)
    private Distributor distributor;

    @ManyToOne
    @JoinColumn(name = "STATUS_ID" , nullable = false)
    private SoStatus soStatus;

    @Column(name = "TAX")
    private Double tax;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "CREATE_TIME")
    private Timestamp createTime;

    @ManyToOne
    @JoinColumn(name = "EMPLOYEE_ID" , nullable = false)
    private Employee employee;

    @Column(name = "DUE_DAY")
    private Timestamp dueDay;

    @Column(name = "DELIVERY_TIME")
    private Timestamp deliveryTime;

    @Column(name = "INVOICENO")
    private String invoiceNo;

    @Column(name = "RECEIVABLE")
    private Double receivable;

    @Column(name = "RECEIVED")
    private Double received;

    @Column(name = "REMARK")
    private String remark;


    public SalesOrder(){}

    public SalesOrder(Distributor distributor, SoStatus soStatus, Double tax, Double amount, Timestamp createTime, Employee employee, Timestamp dueDay, Timestamp deliveryTime, String invoiceNo, Double receivable, Double received) {
        this.distributor = distributor;
        this.soStatus = soStatus;
        this.tax = tax;
        this.amount = amount;
        this.createTime = createTime;
        this.employee = employee;
        this.dueDay = dueDay;
        this.deliveryTime = deliveryTime;
        this.invoiceNo = invoiceNo;
        this.receivable = receivable;
        this.received = received;
    }

    public Long getSoId() {
        return soId;
    }

    public void setSoId(Long soId) {
        this.soId = soId;
    }

    public Distributor getDistributor() {
        return distributor;
    }

    public void setDistributor(Distributor distributor) {
        this.distributor = distributor;
    }

    public SoStatus getSoStatus() {
        return soStatus;
    }

    public void setSoStatus(SoStatus soStatus) {
        this.soStatus = soStatus;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Timestamp getDueDay() {
        return dueDay;
    }

    public void setDueDay(Timestamp dueDay) {
        this.dueDay = dueDay;
    }

    public Timestamp getDeliveryTime() {
        return deliveryTime;
    }

    public void setDeliveryTime(Timestamp deliveryTime) {
        this.deliveryTime = deliveryTime;
    }

    public String getInvoiceNo() {
        return invoiceNo;
    }

    public void setInvoiceNo(String invoiceNo) {
        this.invoiceNo = invoiceNo;
    }

    public Double getReceivable() {
        return receivable;
    }

    public void setReceivable(Double receivable) {
        this.receivable = receivable;
    }

    public Double getReceived() {
        return received;
    }

    public void setReceived(Double received) {
        this.received = received;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "SalesOrder{" +
                "soId=" + soId +
                ", distributor=" + distributor +
                ", soStatus=" + soStatus +
                ", tax=" + tax +
                ", amount=" + amount +
                ", createTime=" + createTime +
                ", employee=" + employee +
                ", dueDay=" + dueDay +
                ", deliveryTime=" + deliveryTime +
                ", invoiceNo='" + invoiceNo + '\'' +
                ", receivable=" + receivable +
                ", received=" + received +
                ", remark='" + remark + '\'' +
                '}';
    }
}
