package com.anhe3d.domain.salesdb;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(schema = "salesdb", name = "SO_STATUS")
public class SoStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "STATUS_ID")
    private Long statusId;

    @Column(name = "UPDATE_TIME")
    private Timestamp updateTime;

    @Column(name = "CREATED")
    private boolean created;

    @Column(name = "APPROVED")
    private boolean approved;

    @Column(name = "EXECUTED")
    private boolean executed;

    @Column(name = "PAYMENT")
    private boolean payment;

    @Column(name = "SHIPMENTS")
    private boolean shipments;

    @Column(name = "INVOICE")
    private boolean invoice;

    @Column(name = "COMPLETED")
    private boolean completed;

    @Column(name = "ENABLED")
    private boolean enabled;

    public SoStatus(){}

    public SoStatus(Timestamp updateTime, boolean created, boolean approved, boolean executed, boolean shipments, boolean payment, boolean invoice, boolean completed, boolean enabled) {
        this.updateTime = updateTime;
        this.created = created;
        this.approved = approved;
        this.executed = executed;
        this.shipments = shipments;
        this.payment = payment;
        this.invoice = invoice;
        this.completed = completed;
        this.enabled = enabled;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isExecuted() {
        return executed;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    public boolean isShipments() {
        return shipments;
    }

    public void setShipments(boolean shipments) {
        this.shipments = shipments;
    }

    public boolean isPayment() {
        return payment;
    }

    public void setPayment(boolean payment) {
        this.payment = payment;
    }

    public boolean isInvoice() {
        return invoice;
    }

    public void setInvoice(boolean invoice) {
        this.invoice = invoice;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "PoStatus{" +
                "statusId=" + statusId +
                ", updateTime=" + updateTime +
                ", created=" + created +
                ", approved=" + approved +
                ", executed=" + executed +
                ", shipments=" + shipments +
                ", payment=" + payment +
                ", invoice=" + invoice +
                ", completed=" + completed +
                ", enabled=" + enabled +
                '}';
    }
}
