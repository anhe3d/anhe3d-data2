package com.anhe3d.domain.testdb;

import javax.persistence.*;

/**
 * Created by kevinhung on 3/24/16.
 */
@Entity
@Table(name = "SHOP_MENU", schema = "testdb")
public class ShopMenu {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ITEM_ID")
    private Long itemId;

    @ManyToOne
    @JoinColumn(name = "SHOP_ID")
    private Shop shop;

    @Column(name = "ITEM_NAME")
    private String itemName;

    @Column(name = "ITEM_PRICE")
    private double itemPrice;

    public ShopMenu() {
    }

    public ShopMenu(Shop shop, String itemName, double itemPrice) {
        this.shop = shop;
        this.itemName = itemName;
        this.itemPrice = itemPrice;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public Shop getShop() {
        return shop;
    }

    public void setShop(Shop shop) {
        this.shop = shop;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    @Override
    public String toString() {
        return "ShopMenu{" +
                "itemId=" + itemId +
                ", shop=" + shop +
                ", itemName='" + itemName + '\'' +
                ", itemPrice=" + itemPrice +
                '}';
    }
}
