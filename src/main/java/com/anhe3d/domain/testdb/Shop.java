package com.anhe3d.domain.testdb;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

/**
 * Created by kevinhung on 3/24/16.
 */
@Entity
@Table(name = "SHOP", schema = "testdb")
public class Shop {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "SHOP_ID")
    private Long shopId;

    @Column(name = "SHOP_NAME")
    private String shopName;

    @Column(name = "SHOP_TELEPHONE")
    private String shopTelephone;

//    @OneToMany(mappedBy = "shop", cascade = CascadeType.MERGE)
//    private Set<ShopMenu> shopMenus = new HashSet<>();

    public Shop() {
    }

    public Shop(String shopName, String shopTelephone) {
        this.shopName = shopName;
        this.shopTelephone = shopTelephone;
    }

    public String getShopName() {
        return shopName;
    }

    public void setShopName(String shopName) {
        this.shopName = shopName;
    }

    public String getShopTelephone() {
        return shopTelephone;
    }

    public void setShopTelephone(String shopTelephone) {
        this.shopTelephone = shopTelephone;
    }

//    public Set<ShopMenu> getShopMenus() {
//        return shopMenus;
//    }
//
//    public void setShopMenus(Set<ShopMenu> shopMenus) {
//        this.shopMenus = shopMenus;
//    }


    @Override
    public String toString() {
        return "Shop{" +
                "shopId=" + shopId +
                ", shopName='" + shopName + '\'' +
                ", shopTelephone='" + shopTelephone + '\'' +
                '}';
    }
}
