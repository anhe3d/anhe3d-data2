package com.anhe3d.domain.testdb;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * Created by USER on 2016/4/20.
 */

@Entity
@Table(name = "ATTENDANCE")

public class Attendance {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ID")
    private Long id;

    @ManyToOne
    @JoinColumn(name = "EMPLOYEE_ID" , nullable = false)
    private Employee employee;

    @Column(name = "CARD_ID", nullable = false)
    private String cardId;


//    private Timestamp starttime;
//    private Timestamp endtime;


    @Column(name = "DATETIME")
    private Timestamp dateTime;

    @Column(name = "COMMENTS")
    private String comment;

    public Attendance(){}

    public Attendance(Employee employee, String cardId) {
        this.employee = employee;
        this.cardId = cardId;
    }

    public Attendance(Employee employee, String cardId, Timestamp dateTime, String comment) {
        this.employee = employee;
        this.cardId = cardId;
        this.dateTime = dateTime;
        this.comment = comment;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public String getCardId() {
        return cardId;
    }

    public void setCardId(String cardId) {
        this.cardId = cardId;
    }

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    @Override
    public String toString() {
        return "Attendance{" +
                "id=" + id +
                ", employee=" + employee +
                ", cardId='" + cardId + '\'' +
                ", time=" + dateTime +
                ", comment='" + comment + '\'' +
                '}';
    }
}
