package com.anhe3d.domain.vendordb;

import com.anhe3d.domain.crmdb.Vendor;
import com.anhe3d.domain.stocksdb.ItemCategory;
import com.anhe3d.domain.stocksdb.ItemType;
import com.anhe3d.domain.stocksdb.ItemUnit;

import javax.persistence.*;
import java.io.File;
import java.sql.Timestamp;

@Entity
@Table(schema = "vendordb", name = "ITEM_TEST")
public class ItemTest {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "ITEM_ID")
    private Long itemId;

    @Column(name = "ITEM_NAME")
    private String itemName;

    @ManyToOne
    @JoinColumn(name = "CO_ID" , nullable = false)
    private Vendor vendor;

    @ManyToOne
    @JoinColumn(name = "ITEM_CATEGORY" , nullable = false)
    private ItemCategory itemCategory;

    @Column(name = "ITEM_PRICE")
    private Double itemPrice;

    @ManyToOne
    @JoinColumn(name = "ITEM_UNIT" , nullable = false)
    private ItemUnit itemUnit;

    @ManyToOne
    @JoinColumn(name = "ITEM_TYPE" , nullable = false)
    private ItemType itemType;

    @Column(name = "ITEM_VENDORNUM")
    private String itemVendorNum;

    @Column(name = "CREATE_TIME")
    private Timestamp createTime;

    @Column(name = "UPDATE_TIME")
    private Timestamp updateTime;

    @Column(name = "ENABLED")
    private Boolean enabled;

    @Column(name = "IMAGE")
    private File image;

    public ItemTest(){}

    public ItemTest(String itemName, Vendor vendor, ItemCategory itemCategory, Double itemPrice, ItemUnit itemUnit, ItemType itemType, String itemVendorNum, Timestamp createTime, Timestamp updateTime, Boolean enabled, File image) {
        this.itemName = itemName;
        this.vendor = vendor;
        this.itemCategory = itemCategory;
        this.itemPrice = itemPrice;
        this.itemUnit = itemUnit;
        this.itemType = itemType;
        this.itemVendorNum = itemVendorNum;
        this.createTime = createTime;
        this.updateTime = updateTime;
        this.enabled = enabled;
        this.image = image;
    }

    public Long getItemId() {
        return itemId;
    }

    public void setItemId(Long itemId) {
        this.itemId = itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public ItemCategory getItemCategory() {
        return itemCategory;
    }

    public void setItemCategory(ItemCategory itemCategory) {
        this.itemCategory = itemCategory;
    }

    public Double getItemPrice() {
        return itemPrice;
    }

    public void setItemPrice(Double itemPrice) {
        this.itemPrice = itemPrice;
    }

    public ItemUnit getItemUnit() {
        return itemUnit;
    }

    public void setItemUnit(ItemUnit itemUnit) {
        this.itemUnit = itemUnit;
    }

    public ItemType getItemType() {
        return itemType;
    }

    public void setItemType(ItemType itemType) {
        this.itemType = itemType;
    }

    public String getItemVendorNum() {
        return itemVendorNum;
    }

    public void setItemVendorNum(String itemVendorNum) {
        this.itemVendorNum = itemVendorNum;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public File getImage() {
        return image;
    }

    public void setImage(File image) {
        this.image = image;
    }

    @Override
    public String toString() {
        return "ItemTest{" +
                "itemId=" + itemId +
                ", itemName='" + itemName + '\'' +
                ", vendor=" + vendor +
                ", itemCategory=" + itemCategory +
                ", itemPrice=" + itemPrice +
                ", itemUnit=" + itemUnit +
                ", itemType=" + itemType +
                ", itemVendorNum='" + itemVendorNum + '\'' +
                ", createTime=" + createTime +
                ", updateTime=" + updateTime +
                ", enabled=" + enabled +
                ", image=" + image +
                '}';
    }
}
