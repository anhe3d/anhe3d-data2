package com.anhe3d.domain.purchasedb;

import com.anhe3d.domain.testdb.Employee;
import com.anhe3d.domain.crmdb.Vendor;

import javax.persistence.*;
import java.sql.Timestamp;

/*
 * @param @Table 輸入DB所在的schema和name
 */
@Entity
@Table(schema = "purchasedb", name = "PURCHASE_ORDER")
public class PurchaseOrder {
    /*
     * 主鍵
     * @param @Column 輸入在DB中的name
     * @param poId 在JAVA中型別為Long
     */
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PO_ID")
    private Long poId;

    /*
     * @param @ManyToOne 多對一，因為VENDOR為此DB的外來鍵
     * @param @JoinColumn 因有設定多對一的關係，所以要打成JoinColumn
     * @param nullable 設定為false，表示不能空值
     */
    @ManyToOne
    @JoinColumn(name = "VENDOR_ID" , nullable = false)
    private Vendor vendor;

    @ManyToOne
    @JoinColumn(name = "STATUS_ID" , nullable = false)
    private PoStatus poStatus;

    @Column(name = "TAX")
    private Double tax;

    @Column(name = "AMOUNT")
    private Double amount;

    @Column(name = "CREATE_TIME")
    private Timestamp createTime;

    @ManyToOne
    @JoinColumn(name = "EMPLOYEE_ID" , nullable = false)
    private Employee employee;

    @Column(name = "DUE_DAY")
    private Timestamp dueDay;

    @Column(name = "STOCK_WHETHER")
    private boolean stockWhether;

    @Column(name = "REMARK")
    private String remark;


    //設定一個空的PurchaseOrder
    public PurchaseOrder (){}

    //constructor:設定必須輸入的Field即可
    public PurchaseOrder(Vendor vendor, PoStatus poStatus, Double tax, Double amount, Timestamp createTime, Employee employee, Timestamp dueDay, boolean stockWhether) {
        this.vendor = vendor;
        this.poStatus = poStatus;
        this.tax = tax;
        this.amount = amount;
        this.createTime = createTime;
        this.employee = employee;
        this.dueDay = dueDay;
        this.stockWhether = stockWhether;
    }

    //Getter,Setter
    public Long getPoId() {
        return poId;
    }

    public void setPoId(Long poId) {
        this.poId = poId;
    }

    public Vendor getVendor() {
        return vendor;
    }

    public void setVendor(Vendor vendor) {
        this.vendor = vendor;
    }

    public PoStatus getPoStatus() {
        return poStatus;
    }

    public void setPoStatus(PoStatus poStatus) {
        this.poStatus = poStatus;
    }

    public Double getTax() {
        return tax;
    }

    public void setTax(Double tax) {
        this.tax = tax;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Timestamp getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Timestamp createTime) {
        this.createTime = createTime;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Timestamp getDueDay() {
        return dueDay;
    }

    public void setDueDay(Timestamp dueDay) {
        this.dueDay = dueDay;
    }

    public boolean isStockWhether() {
        return stockWhether;
    }

    public void setStockWhether(boolean stockWhether) {
        this.stockWhether = stockWhether;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    //toString


    @Override
    public String toString() {
        return "PurchaseOrder{" +
                "poId=" + poId +
                ", vendor=" + vendor +
                ", poStatus=" + poStatus +
                ", tax=" + tax +
                ", amount=" + amount +
                ", createTime=" + createTime +
                ", employee=" + employee +
                ", dueDay=" + dueDay +
                ", stockWhether=" + stockWhether +
                ", remark='" + remark + '\'' +
                '}';
    }
}
