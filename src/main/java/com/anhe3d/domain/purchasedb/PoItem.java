package com.anhe3d.domain.purchasedb;

import com.anhe3d.domain.stocksdb.Item;

import javax.persistence.*;
/**
 *
 */

@Entity
@Table(schema = "purchasedb", name = "PO_ITEM")
public class PoItem {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "POITEM_ID")
    private Long poItemId;

    @ManyToOne
    @JoinColumn(name = "PO_ID" , nullable = false)
    private PurchaseOrder purchaseOrder;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID" , nullable = false)
    private Item item;

    @Column(name = "QUANTITY")
    private int quantity;

    @Column(name = "PRICE")
    private Double price;

    public PoItem(){}

    public PoItem(PurchaseOrder purchaseOrder, Item item, int quantity, Double price) {
        this.purchaseOrder = purchaseOrder;
        this.item = item;
        this.quantity = quantity;
        this.price = price;
    }

    public Long getPoItemId() {
        return poItemId;
    }

    public void setPoItemId(Long poItemId) {
        this.poItemId = poItemId;
    }

    public PurchaseOrder getPurchaseOrder() {
        return purchaseOrder;
    }

    public void setPurchaseOrder(PurchaseOrder purchaseOrder) {
        this.purchaseOrder = purchaseOrder;
    }

    public Item getItem() {
        return item;
    }

    public void setItem(Item item) {
        this.item = item;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "PoItem{" +
                "poItemId=" + poItemId +
                ", purchaseOrder=" + purchaseOrder +
                ", item=" + item +
                ", quantity=" + quantity +
                ", price=" + price +
                '}';
    }
}
