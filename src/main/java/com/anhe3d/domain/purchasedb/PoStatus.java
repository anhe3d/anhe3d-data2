package com.anhe3d.domain.purchasedb;

import javax.persistence.*;
import java.sql.Timestamp;


@Entity
@Table(schema = "purchasedb", name = "PO_STATUS")
public class PoStatus {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "STATUS_ID")
    private Long statusId;

    @Column(name = "UPDATE_TIME")
    private Timestamp updateTime;

    @Column(name = "CREATED")
    private boolean created;

    @Column(name = "APPROVED")
    private boolean approved;

    @Column(name = "EXECUTED")
    private boolean executed;

    @Column(name = "GOOD_RECEIPT")
    private boolean goodReceipt;

    @Column(name = "STOCK_IN")
    private boolean stockIn;

    @Column(name = "PAYMENT")
    private boolean payment;

    @Column(name = "INVOICE_RECEIVED")
    private boolean invoiceReceipt;

    @Column(name = "COMPLETED")
    private boolean completed;

    @Column(name = "ENABLED")
    private boolean enabled;

    public PoStatus(){}

    public PoStatus(Timestamp updateTime, boolean created, boolean approved, boolean executed, boolean goodReceipt, boolean stockIn, boolean payment, boolean invoiceReceipt, boolean completed, boolean enabled) {
        this.updateTime = updateTime;
        this.created = created;
        this.approved = approved;
        this.executed = executed;
        this.goodReceipt = goodReceipt;
        this.stockIn = stockIn;
        this.payment = payment;
        this.invoiceReceipt = invoiceReceipt;
        this.completed = completed;
        this.enabled = enabled;
    }

    public Long getStatusId() {
        return statusId;
    }

    public void setStatusId(Long statusId) {
        this.statusId = statusId;
    }

    public Timestamp getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Timestamp updateTime) {
        this.updateTime = updateTime;
    }

    public boolean isCreated() {
        return created;
    }

    public void setCreated(boolean created) {
        this.created = created;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public boolean isExecuted() {
        return executed;
    }

    public void setExecuted(boolean executed) {
        this.executed = executed;
    }

    public boolean isGoodReceipt() {
        return goodReceipt;
    }

    public void setGoodReceipt(boolean goodReceipt) {
        this.goodReceipt = goodReceipt;
    }

    public boolean isStockIn() {
        return stockIn;
    }

    public void setStockIn(boolean stockIn) {
        this.stockIn = stockIn;
    }

    public boolean isPayment() {
        return payment;
    }

    public void setPayment(boolean payment) {
        this.payment = payment;
    }

    public boolean isInvoiceReceipt() {
        return invoiceReceipt;
    }

    public void setInvoiceReceipt(boolean invoiceReceipt) {
        this.invoiceReceipt = invoiceReceipt;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    @Override
    public String toString() {
        return "PoStatus{" +
                "statusId=" + statusId +
                ", updateTime=" + updateTime +
                ", created=" + created +
                ", approved=" + approved +
                ", executed=" + executed +
                ", goodReceipt=" + goodReceipt +
                ", stockIn=" + stockIn +
                ", payment=" + payment +
                ", invoiceReceipt=" + invoiceReceipt +
                ", completed=" + completed +
                ", enabled=" + enabled +
                '}';
    }
}
