package com.anhe3d.repository.salesdb;

import com.anhe3d.domain.salesdb.SalesItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesItemRepository extends CrudRepository<SalesItem, Long> {
//    Iterable<SalesItem> findAllByDistributor(Distributor distributor);
    SalesItem findBySItemId (Long sItemId);
}
