package com.anhe3d.repository.salesdb;

import com.anhe3d.domain.salesdb.SalesOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesOrderRepository extends CrudRepository<SalesOrder, Long> {
    SalesOrder findByInvoiceNo (String invoiceNo);
}
