package com.anhe3d.repository.salesdb;
import com.anhe3d.domain.salesdb.SoStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SoStatusRepository extends CrudRepository<SoStatus, Long>{
}
