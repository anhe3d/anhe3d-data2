package com.anhe3d.repository.salesdb;
import com.anhe3d.domain.salesdb.SalesOrder;
import com.anhe3d.domain.salesdb.SoItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SoItemRepository extends CrudRepository<SoItem, Long>{
    Iterable<SoItem> findAllBySalesOrder(SalesOrder salesOrder);

}
