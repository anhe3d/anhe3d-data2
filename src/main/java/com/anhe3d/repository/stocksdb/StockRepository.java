package com.anhe3d.repository.stocksdb;

import com.anhe3d.domain.stocksdb.Item;
import com.anhe3d.domain.stocksdb.Stock;
import com.anhe3d.domain.vendordb.Testview;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.List;

@Repository
public interface StockRepository extends CrudRepository<Stock, Long>{
    Stock findByItem (Item item);
//    @Query(value = "SELECT * FROM stocksdb.stockview", nativeQuery = true)
//    List<Object[]> testQuery();
//
//    @Query(value = "SELECT * FROM stocksdb.stocksview", nativeQuery = true)
//    List<Object[]> step1();

    @Query(value = "SELECT * FROM stocksdb.stocksview", nativeQuery = true)
    List<Stock> step2();


    @Query(value = "CALL test;", nativeQuery = true)
    List<Object[]> testQuery1();

}
