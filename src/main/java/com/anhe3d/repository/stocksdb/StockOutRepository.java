package com.anhe3d.repository.stocksdb;

import com.anhe3d.domain.stocksdb.Item;
import com.anhe3d.domain.stocksdb.StockOut;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StockOutRepository  extends CrudRepository<StockOut, Long>{
    Iterable<StockOut> findAllByItem(Item item);

}
