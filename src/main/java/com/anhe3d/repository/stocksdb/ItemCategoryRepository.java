package com.anhe3d.repository.stocksdb;

import com.anhe3d.domain.stocksdb.ItemCategory;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemCategoryRepository extends CrudRepository<ItemCategory, Long>{
}
