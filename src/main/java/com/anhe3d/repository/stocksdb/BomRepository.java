package com.anhe3d.repository.stocksdb;

import com.anhe3d.domain.stocksdb.Bom;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
@Repository
public interface BomRepository extends CrudRepository<Bom, Long>{
    Bom findByBomName (String sitemName);
    Iterable<Bom> findAllByBomName(String bomName);
    Iterable<Bom> findAllByBom(Bom bom);


}
