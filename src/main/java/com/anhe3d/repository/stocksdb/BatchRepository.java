package com.anhe3d.repository.stocksdb;

import com.anhe3d.domain.stocksdb.Batch;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BatchRepository extends CrudRepository<Batch, Long>{
}
