package com.anhe3d.repository.stocksdb;

import com.anhe3d.domain.stocksdb.ItemUnit;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemUnitRepository extends CrudRepository<ItemUnit, Long>{
}
