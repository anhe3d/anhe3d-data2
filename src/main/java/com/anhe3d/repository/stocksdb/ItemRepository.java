package com.anhe3d.repository.stocksdb;



import com.anhe3d.domain.crmdb.Vendor;
import com.anhe3d.domain.stocksdb.Item;
import com.anhe3d.domain.stocksdb.ItemType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemRepository extends CrudRepository<Item, Long>{
    Iterable<Item> findAllByVendor(Vendor vendor);
    Iterable<Item> findAllByItemType(ItemType itemType);

//    Iterable<SalesItem> findAllByItemId(Long ItemId);

}
