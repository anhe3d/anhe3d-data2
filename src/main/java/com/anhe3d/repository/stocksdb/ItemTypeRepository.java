package com.anhe3d.repository.stocksdb;

import com.anhe3d.domain.stocksdb.ItemType;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemTypeRepository extends CrudRepository<ItemType, Long>{
}
