package com.anhe3d.repository.purchasedb;

import com.anhe3d.domain.purchasedb.PoStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

@Repository
public interface PoStatusRepository extends CrudRepository<PoStatus, Long>{
        PoStatus findByUpdateTime(Timestamp updateTime);
}
