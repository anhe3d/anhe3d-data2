package com.anhe3d.repository.purchasedb;

import com.anhe3d.domain.purchasedb.PoItem;
import com.anhe3d.domain.purchasedb.PurchaseOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

//介接JAVA和SQL，用來尋找，新增，刪除的操作，若需要新的方法，可自行輸入
@Repository
public interface PoItemRepository extends CrudRepository<PoItem, Long>{
    Iterable<PoItem> findAllByPurchaseOrder(PurchaseOrder purchaseOrder);
    PoItem findByPurchaseOrder(PurchaseOrder purchaseOrder);
    Iterable<Double> findAllByPrice(Double price);
    PoItem findByPrice (Double price);



}
