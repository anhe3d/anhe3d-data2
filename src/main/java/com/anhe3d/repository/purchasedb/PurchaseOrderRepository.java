package com.anhe3d.repository.purchasedb;

import com.anhe3d.domain.purchasedb.PurchaseOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Timestamp;

/*
 * 介接JAVA和SQL，用來尋找，新增，刪除的操作，若需要新的方法，可自行輸入
 * findBy_____  利用某個物件來找尋資料，例如:"PurchaseOrder findByCreateTime (Timestamp createTime );"，
   就是利用createTime，來找尋符合此createTime的PurchaseOrder
 * findAllBy____ 需要先加入集合型別，找尋並列出所有符合的資料，例如:"Iterable<PoItem> findAllByPurchaseOrder(PurchaseOrder purchaseOrder);
   "，利用某個purchaseOrder，找尋所有有此purchaseOrder的資料(PoItem)
 */
@Repository
public interface PurchaseOrderRepository extends CrudRepository<PurchaseOrder, Long>{
//    PurchaseOrder findByStatusId (PoStatus poStatus);
    PurchaseOrder findByCreateTime (Timestamp createTime );
}
