package com.anhe3d.repository.vendordb;


import com.anhe3d.domain.vendordb.ItemTest;
import com.anhe3d.domain.crmdb.Vendor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ItemTestRepository extends CrudRepository<ItemTest, Long>{
    Iterable<ItemTest> findAllByVendor(Vendor vendor);
}
