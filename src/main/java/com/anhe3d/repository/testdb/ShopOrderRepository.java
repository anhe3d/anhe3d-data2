package com.anhe3d.repository.testdb;

import com.anhe3d.domain.testdb.ShopOrder;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by kevinhung on 3/24/16.
 */
@Repository
public interface ShopOrderRepository extends CrudRepository<ShopOrder, Long>{
}
