package com.anhe3d.repository.testdb;

import com.anhe3d.domain.testdb.Shop;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by kevinhung on 3/24/16.
 */
@Repository
public interface ShopRepository extends CrudRepository<Shop, Long>{

    Shop findByShopName(String shopName);

    @Query("SELECT shop FROM Shop shop")
    List<Shop> testQuery();

    @Query("SELECT shop.shopName, shop.shopTelephone FROM Shop shop")
    List<Object[]> testObjectQuery();
}
