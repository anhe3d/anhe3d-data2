package com.anhe3d.repository.testdb;

import com.anhe3d.domain.testdb.Shop;
import com.anhe3d.domain.testdb.ShopMenu;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by kevinhung on 3/24/16.
 */
@Repository
public interface ShopMenuRepository extends CrudRepository<ShopMenu, Long> {

    @Query("SELECT shopMenu FROM ShopMenu shopMenu")
    List<ShopMenu> testQuery();

    @Query("SELECT shopMenu.itemName, shop.shopName FROM ShopMenu shopMenu, Shop shop WHERE shopMenu.shop.shopId = shop.shopId")
    List<Object[]> testQueryAdvanced();

    Iterable<ShopMenu> findAllByShop(Shop shop);

    ShopMenu findByItemName(String itemName);
}
