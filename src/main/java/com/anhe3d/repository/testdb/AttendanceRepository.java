package com.anhe3d.repository.testdb;

import com.anhe3d.domain.testdb.Attendance;
import com.anhe3d.domain.testdb.Employee;
import org.springframework.data.repository.CrudRepository;

import java.sql.Timestamp;

public interface AttendanceRepository extends CrudRepository<Attendance, Long> {
    Attendance findByEmployee(Employee employee);
    Iterable<Attendance> findAllByEmployee(Employee employee);
//    Iterable< Attendance> findByDate (java.util.Date date);
//    Iterable< Attendance> findByDateTime (Timestamp dateTime);
//    Iterable< Attendance> findByDateTime2 (LocalDateTime localDateTime);
    Attendance findByDateTime(Timestamp timestamp);
//    Attendance findByDateTime2(LocalDateTime localDateTime);





}
