package com.anhe3d.repository.testdb;

import com.anhe3d.domain.testdb.Employee;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository

public interface EmployeeRepository extends CrudRepository<Employee, Long> {

    Employee findByLastName(String lastName);

    Employee findByFirstName(String firstName);

    Employee findByCellphone(String cellphone);

    Employee findByEmployeeId(String employeeId);

}