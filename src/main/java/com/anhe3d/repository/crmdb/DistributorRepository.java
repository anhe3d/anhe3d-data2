package com.anhe3d.repository.crmdb;
import com.anhe3d.domain.crmdb.Distributor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface DistributorRepository extends CrudRepository<Distributor, Long>{
    Distributor findByDtName (String dtName);

}
