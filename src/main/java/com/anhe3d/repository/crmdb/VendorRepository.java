package com.anhe3d.repository.crmdb;

import com.anhe3d.domain.crmdb.Vendor;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VendorRepository extends CrudRepository<Vendor, Long> {
    Vendor findByVendorName(String vendorName);

}
