package com.anhe3d.config;

import org.springframework.boot.autoconfigure.jdbc.DataSourceBuilder;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import javax.sql.DataSource;

/**
 * Created by USER on 2016/6/7.
 */

/*
 * @param @ConfigurationProperties prefix輸入在application.properties的DB名稱
 * 可讓Spring Server 同時使用多個DB
 */
@Configuration
public class DatasourceConfig {
    @Bean
    @Primary
    @ConfigurationProperties(prefix = "datasource.purchasedb")
    public DataSource purchaseDB() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource.vendordb")
    public DataSource vendorDB() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource.testdb")
    public DataSource testDB() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource.stocksdb")
    public DataSource stocksDB() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource.salesdb")
    public DataSource salesDB() {
        return DataSourceBuilder.create().build();
    }

    @Bean
    @ConfigurationProperties(prefix = "datasource.crmdb")
    public DataSource crmDB() {
        return DataSourceBuilder.create().build();
    }
}
