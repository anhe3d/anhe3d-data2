package com.anhe3d.web.api;

import com.anhe3d.domain.purchasedb.PoItem;
import com.anhe3d.domain.purchasedb.PurchaseOrder;
import com.anhe3d.domain.stocksdb.*;
import com.anhe3d.domain.testdb.Employee;
import com.anhe3d.repository.purchasedb.PoItemRepository;
import com.anhe3d.repository.purchasedb.PurchaseOrderRepository;
import com.anhe3d.repository.stocksdb.*;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.*;

@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.2.114", "http://anhe-internal.local"})
@RestController
@RequestMapping(path = "/api/stock")
public class StockController {
    private static final Logger logger = LoggerFactory.getLogger(StockController.class);
    @Autowired
    private BatchRepository batchRepository;
    @Autowired
    private StockRepository stockRepository;
    @Autowired
    private ItemCategoryRepository itemCategoryRepository;
    @Autowired
    private StockOutRepository stockOutRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    private PoItemRepository poItemRepository;
    @Autowired
    private ItemTypeRepository itemTypeRepository;

    @RequestMapping(
            path = "/batches",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getBatches() {
        Collection<Batch> Batches = (Collection<Batch>) batchRepository.findAll();
        logger.info("getBatches");
        return new ResponseEntity<>(Batches, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/stocks",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getStocks() {
        Collection<Stock> stocks = (Collection<Stock>) stockRepository.findAll();
        logger.info("getStocks");
        return new ResponseEntity<>(stocks, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/itemCategories",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getItemCategory() {
        Collection<ItemCategory> itemCategories = (Collection<ItemCategory>) itemCategoryRepository.findAll();
        logger.info("getCategories");
        return new ResponseEntity<>(itemCategories, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/stockOuts",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getStockOuts() {
        Collection<StockOut> stockOuts = (Collection<StockOut>) stockOutRepository.findAll();
        logger.info("getStocks");
        return new ResponseEntity<>(stockOuts, HttpStatus.OK);
    }

//    @RequestMapping(
//            path = "/stock",
//            method = RequestMethod.POST,
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Stock> createStoragePlace(@RequestBody Stock stock) {
//        stockRepository.save(stock);
//        logger.info("createStock");
//        return new ResponseEntity<>(stock, HttpStatus.CREATED);
//    }


    @RequestMapping(
            path = "/batch",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Batch> createBatch(@RequestBody Batch batch) {
        batchRepository.save(batch);
        logger.info("createBatch");
        return new ResponseEntity<>(batch, HttpStatus.CREATED);
    }

    @RequestMapping(
            path = "/stock",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Stock> createStock(@RequestBody Stock stock) {
        stockRepository.save(stock);
        logger.info("createStock");
        return new ResponseEntity<>(stock, HttpStatus.CREATED);
    }

    @RequestMapping(
            path = "/stockOutAll",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StockOut> createAllStockOut(@RequestBody String json)  throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        JSONObject obj = new JSONObject(json);
        JSONArray stockOuts = obj.getJSONArray("stockOuts");
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
//        System.out.println(stockOuts.length());
        for (int i = 0; i < stockOuts.length(); i++) {
            StockOut stockOut = mapper.readValue(stockOuts.getJSONObject(i).toString(), StockOut.class);
            System.out.println(stockOut);
            stockOut.setTime(timestamp);
            stockOutRepository.save(stockOut);
        }
        return new ResponseEntity<StockOut>(stockOutRepository.findOne(1L),HttpStatus.CREATED);
    }

    @RequestMapping(
            path = "/stock",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Stock> updateStock(@RequestBody Stock stock) {
        Stock newStock = stockRepository.save(stock);
        logger.info("updateStock");
        return new ResponseEntity<>(newStock, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/stockOuts",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StockOut> createStockOut(@RequestBody StockOut stockOut) {
//        Timestamp timestamp = new Timestamp(new Date().getTime());
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        stockOut.setTime(timestamp);
        stockOutRepository.save(stockOut);
        logger.info("createStockOut");
        return new ResponseEntity<>(stockOut, HttpStatus.CREATED);
    }

    @RequestMapping(
            path = "/stockOuts/{poId}",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<StockOut> createStockOutFromPuchaseOrder(@PathVariable("poId") Long poId) {

        PurchaseOrder purchaseOrder = purchaseOrderRepository.findOne(poId);
        List<PoItem> poItems = new ArrayList<>();
        poItemRepository.findAllByPurchaseOrder(purchaseOrder).forEach(poItems::add);
        for (PoItem poItem : poItems) {
            StockOut stockOut = new StockOut();
            Employee employee = purchaseOrder.getEmployee();
            Timestamp timestamp = new Timestamp(new Date().getTime());
            Timestamp timestamp1 = Timestamp.valueOf(LocalDateTime.now());
            stockOut.setTime(timestamp1);
            stockOut.setCreateTime(timestamp);
            stockOut.setEmployee(employee);
            stockOut.setItem(poItem.getItem());
            stockOut.setPurchaseOrder(purchaseOrder);
            stockOut.setQuantity(poItem.getQuantity());
            stockOutRepository.save(stockOut);
        }
        logger.info("createStockOutFromPuchaseOrder");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @RequestMapping(
            path = "/stock/{itemId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getQuantityByItemId(@PathVariable("itemId") Long itemId) {
        Item item = itemRepository.findOne(itemId);
        List<StockOut> stockOuts = new ArrayList<>();
        stockOutRepository.findAllByItem(item).forEach(stockOuts::add);
        int tempQuantity = 0;
        for (StockOut stockOut : stockOuts) {
            int k = stockOut.getQuantity();
            tempQuantity = tempQuantity + k;
        }
        Stock stock = stockRepository.findByItem(item);
        stock.setQuantity(stock.getQuantity() + tempQuantity);
        stockRepository.save(stock);
        return new ResponseEntity<>(stock, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/stockOut/{poId}/{itemId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getQuantityByItemIdAndPoId(@PathVariable("poId") Long poId, @PathVariable("itemId") Long itemId) {
        Item item = itemRepository.findOne(itemId);
        List<StockOut> stockOuts = new ArrayList<>();
        List<StockOut> stockOutsB = new ArrayList<>();
        stockOutRepository.findAllByItem(item).forEach(stockOuts::add);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findOne(poId);
        int tempQuantity = 0;
        for (StockOut stockOut : stockOuts) {
            if (stockOut.getPurchaseOrder() == purchaseOrder) {
                int k = stockOut.getQuantity();
//                stockOutsB.add(stockOut);
                tempQuantity = tempQuantity + k;
            }
        }
        return new ResponseEntity<>(tempQuantity, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/stockOut/{itemId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getQuantityByItemIdFromEachPo(@PathVariable("itemId") Long itemId) {
        Item item = itemRepository.findOne(itemId);
        List<StockOut> stockOuts = new ArrayList<>();
        List<StockOut> stockOutsA = new ArrayList<>();
        stockOutRepository.findAllByItem(item).forEach(stockOuts::add);
        HashMap<PurchaseOrder, Integer> menuMap1 = new HashMap<>();
        while (stockOuts.size() > 0) {
            int tempQuantity = 0;
            for (StockOut stockOut1 : stockOuts) {
                StockOut stockOut = stockOuts.get(0);
                if (stockOut1.getPurchaseOrder() == stockOut.getPurchaseOrder()) {
                    stockOutsA.add(stockOut1);
                }
            }
            for (StockOut aStockOutsA : stockOutsA) {
                stockOuts.remove(aStockOutsA);
                tempQuantity = tempQuantity + aStockOutsA.getQuantity();
            }
            System.out.println(tempQuantity);
            if (tempQuantity > 0) {
                menuMap1.put(stockOutsA.get(0).getPurchaseOrder(), tempQuantity);
            }
            stockOutsA.clear();
        }
        return new ResponseEntity<>(menuMap1, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/stocks/update",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getQuantityOfStock() {
        List<Stock> stocks = new ArrayList<>();
        stockRepository.findAll().forEach(stocks::add);
        for (Stock stock1 : stocks) {
            Item item = itemRepository.findOne(stock1.getItem().getItemId());
            List<StockOut> stockOuts = new ArrayList<>();
            stockOutRepository.findAllByItem(item).forEach(stockOuts::add);
            int tempQuantity = 0;
            for (StockOut stockOut : stockOuts) {
                int k = stockOut.getQuantity();
                tempQuantity = tempQuantity + k;
            }
            Stock stock = stockRepository.findByItem(item);
            stock.setQuantity(tempQuantity);
            stockRepository.save(stock);
        }
        return new ResponseEntity<>(stocks, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/stocks/view2",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getQuantityOfStockView2() {
        List<Stock>list = stockRepository.step2();
        return new ResponseEntity<>(list, HttpStatus.OK);
    }
    @RequestMapping(
            path = "/stocks/item-type3",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getItemOfItemType3() {
        ItemType itemType = itemTypeRepository.findOne(3L);
        List<Item>list = new ArrayList<>();
        itemRepository.findAllByItemType(itemType).forEach(list::add);
        return new ResponseEntity<>(list, HttpStatus.OK);
    }



}