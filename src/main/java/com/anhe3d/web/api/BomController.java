package com.anhe3d.web.api;

import com.anhe3d.domain.stocksdb.Bom;
import com.anhe3d.repository.stocksdb.BomRepository;
import com.anhe3d.repository.stocksdb.StockRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;


@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.2.114", "http://anhe-internal.local"})
@RestController
@RequestMapping(path = "/api/bom")
public class BomController {
    private static final Logger logger = LoggerFactory.getLogger(StockController.class);
    @Autowired
    private StockRepository stockRepository;
    @Autowired
    private BomRepository bomRepository;


    @RequestMapping(
            path = "/{bomId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getBomsByBomId(@PathVariable("bomId") Long bomId) {
        Bom bom = bomRepository.findOne(bomId);
        Iterable<Bom> boms = bomRepository.findAllByBom(bom);
        logger.info("getBoms  ");
        return new ResponseEntity<>(boms, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/all/{bomId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllBomsByBomId(@PathVariable("bomId") Long bomId) {
        Bom bom = bomRepository.findOne(bomId);
        List<Bom> bomList = new ArrayList<>();
        List<Bom> bomList1 = new ArrayList<>();
        bomRepository.findAllByBom(bom).forEach(bomList::add);
        System.out.println(bomList.size());
        for (Bom aBomList : bomList) {
            bomRepository.findAllByBom(aBomList).forEach(bomList1::add);
        }
        System.out.println(bomList1.size());
        logger.info("getBoms");
        return new ResponseEntity<>(bomList1, HttpStatus.OK);
    }
}