package com.anhe3d.web.api;

import com.anhe3d.domain.purchasedb.PoItem;
import com.anhe3d.domain.purchasedb.PoStatus;
import com.anhe3d.domain.purchasedb.PurchaseOrder;
import com.anhe3d.domain.stocksdb.Item;
import com.anhe3d.domain.stocksdb.ItemCategory;
import com.anhe3d.domain.crmdb.Vendor;
import com.anhe3d.repository.purchasedb.PoItemRepository;
import com.anhe3d.repository.purchasedb.PoStatusRepository;
import com.anhe3d.repository.purchasedb.PurchaseOrderRepository;
import com.anhe3d.repository.stocksdb.ItemCategoryRepository;
import com.anhe3d.repository.stocksdb.ItemRepository;
import com.anhe3d.repository.crmdb.VendorRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Collection;
import java.util.Date;

@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.2.114", "http://anhe-internal.local"})
@RestController
@RequestMapping(path = "/api/purchase-order")
public class PurchaseOrderController {
    private static final Logger logger = LoggerFactory.getLogger(PurchaseOrderController.class);
    @Autowired
    private VendorRepository vendorRepository;
    @Autowired
    private ItemCategoryRepository itemCategoryRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    PoItemRepository poItemRepository;
    @Autowired
    PoStatusRepository poStatusRepository;


    /*
     * 取得全部廠商
     * @param GET 從資料庫取得資料
     * @param produces JSON型態
     * @return Collection<Vendor> vendors
     */
    @RequestMapping(
            path = "/vendors",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getVendors() {
        Collection<Vendor> vendors = (Collection<Vendor>) vendorRepository.findAll();
        logger.info("getVendors");
        return new ResponseEntity<>(vendors, HttpStatus.OK);
    }
    /*
     * 取得某廠商的所有商品
     * @param GET 從資料庫取得資料
     * @param produces JSON型態
     * @param {vendor} 網址輸入想查詢的廠商
     * @param @PathVariable 讀取網址中的"vendor"
     * @param vendor1 為對應的廠商
     * @param items 先查到廠商再查所有商品
     * @return Collection<SalesItem> items
     */

    @RequestMapping(
            path = "/item/{vendor}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getItemByVendor(@PathVariable("vendor") String vendor) {
        Vendor vendor1 = vendorRepository.findByVendorName(vendor);
        Collection<Item> items = (Collection<Item>) itemRepository.findAllByVendor(vendor1);
        return new ResponseEntity<>(items, HttpStatus.OK);
    }


    /*
     * 取得某購買單的所有購買商品
     * @param GET 從資料庫取得資料
     * @param produces JSON型態
     * @param {poid} 網址輸入想查詢購買單編號
     * @param @PathVariable 讀取網址中的"poId"
     * @param poId 為網址輸入的購買單編號
     * @param poItems 先查詢poId為哪個購買單，再查詢所有購賣商品
     * @return Collection<PoItems> poItems
     */
    @RequestMapping(
            path = "/po-item/{poId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getPoItemByPoId(@PathVariable("poId") Long poId) throws ParseException {
//            Timestamp d1 = Timestamp.valueOf(timestamp);
//            PurchaseOrder purchaseOrder = purchaseOrderRepository.findByCreateTime(d1);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findOne(poId);
        Iterable<PoItem> poItems = poItemRepository.findAllByPurchaseOrder(purchaseOrder);
        logger.info("getAllPoItem");
        return new ResponseEntity<>(poItems, HttpStatus.OK);
    }

    /*
     * 取得全部購買單
     * @param GET 從資料庫取得資料
     * @param produces JSON型態
     * @return Collection<PurchaseOrder> purchaseOrders
     */
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllPurchaseOrder() {
        Collection<PurchaseOrder> purchaseOrders = (Collection<PurchaseOrder>) purchaseOrderRepository.findAll();
        logger.info("getAllPurchaseOrder");
        return new ResponseEntity<>(purchaseOrders, HttpStatus.OK);
    }


    /*
     * 增加購買單狀態
     * @param PUT 新增資料
     * @param consumes 讓資料寫入，JSON型態
     * @param produces JSON型態
     * @param @RequestBody 接整個object
     * @param poStatus 要寫入的postatus
     * @return newPoStatus 儲存寫入的資料
     */
    @RequestMapping(
            path = "/po-status",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PoStatus> updatePoStatus(@RequestBody PoStatus poStatus) {
        PoStatus newPoStatus = poStatusRepository.save(poStatus);
        logger.info("updatePoStatus");
        return new ResponseEntity<>(newPoStatus, HttpStatus.OK);
    }


    /*
     * 取得全部品項類別
     * @param GET 從資料庫取得資料
     * @param produces JSON型態
     * @return Collection<ItemCategories> itemCategories
     */
    @RequestMapping(
            path = "/item-categories",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllItemCategory() {
        Collection<ItemCategory> itemCategories = (Collection<ItemCategory>) itemCategoryRepository.findAll();
        logger.info("getAllItemCategory");
        return new ResponseEntity<>(itemCategories, HttpStatus.OK);
    }


    /*
     * 新增購買商品
     * @param POST 新增資料到資料庫
     * @param consumes 讓資料寫入，JSON型態
     * @param produces JSON型態
     * @param @RequestBody 接整個object
     * @param poItem 要寫入的poItem
     * @return 儲存寫入的資料
     */
    @RequestMapping(
            path = "/po-item",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PoItem> createPoItem(@RequestBody PoItem poItem) {
        poItemRepository.save(poItem);
        logger.info("createPoItem");
        return new ResponseEntity<>(poItem, HttpStatus.CREATED);
    }

//    @RequestMapping(
//            method = RequestMethod.POST,
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<PurchaseOrder> createPurchaseOrder(@RequestBody PurchaseOrder purchaseOrder) {
//        Timestamp timestamp = new Timestamp(new Date().getTime());
//        PoStatus newPoStatus = purchaseOrder.getPoStatus();
//        newPoStatus.setUpdateTime(timestamp);
//        poStatusRepository.save(newPoStatus);
//
//        purchaseOrder.setCreateTime(timestamp);
//        PurchaseOrder savePurchaseOrder = purchaseOrderRepository.save(purchaseOrder);
//        logger.info("createPurchaseOrder");
//        return new ResponseEntity<PurchaseOrder>(savePurchaseOrder, HttpStatus.CREATED);
//    }


    /*
     * 新增完整請購單，包含purchaseOrder，poStatus及poItems
     * @param POST 新增資料到資料庫
     * @param consumes 讓資料寫入，JSON型態
     * @param produces JSON型態
     * @param @RequestBody 接整個object
     * @param json 把輸入的RequestBody轉為String，此json包含PurchaseOrder及poItems
     * @param mapper ObjectMapper型別
     * @param obj JSONObject型別，並將@RequestBody的json接入
     * @param purchaseOrder PurchaseOrder型別，將purchaseOrder的資料從obj中取出，找到"purchaseOrder"，並轉成PurchaseOrder
     * @param timestamp 設定當下的時間
     * @param poStatus 創一個新的poStatus，並設定timestamp，用poStatusRepository儲存至資料庫
     * @param savedPurchaseOrder ，創一個新的purchaseOrder，並設定timestamp及PoStatus，purchaseOrderRepository用儲存至資料庫
     * @param poItems JSONArray型別，將資料從obj中取出，找到"poItems"
     * @param for 把poItems中的資料一一儲存至資料庫，並設定PurchaseOrder
     * @return 儲存寫入的資料，並顯示savedPurchaseOrder
     */
    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PurchaseOrder> createPurchaseOrderAll(@RequestBody String json) throws IOException {
        logger.info("createPurchaseOrderAll");

        ObjectMapper mapper = new ObjectMapper();
        JSONObject obj = new JSONObject(json);

        PurchaseOrder purchaseOrder = mapper.readValue(obj.getJSONObject("purchaseOrder").toString(), PurchaseOrder.class);
        System.out.println(purchaseOrder);
        Timestamp timestamp = new Timestamp(new Date().getTime());
        PoStatus poStatus = purchaseOrder.getPoStatus();
        poStatus.setUpdateTime(timestamp);
        poStatusRepository.save(poStatus);
        purchaseOrder.setCreateTime(timestamp);
        purchaseOrder.setPoStatus(poStatusRepository.findOne(poStatus.getStatusId()));
        PurchaseOrder savedPurchaseOrder = purchaseOrderRepository.save(purchaseOrder);

        JSONArray poItems = obj.getJSONArray("poItems");
        System.out.println(poItems.length());
        for (int i = 0; i < poItems.length(); i++) {
            PoItem poItem = mapper.readValue(poItems.getJSONObject(i).toString(), PoItem.class);
            System.out.println(poItem);
            poItem.setPurchaseOrder(savedPurchaseOrder);
            poItemRepository.save(poItem);
        }
        return new ResponseEntity<PurchaseOrder>(savedPurchaseOrder,HttpStatus.CREATED);
    }
}