package com.anhe3d.web.api;

import com.anhe3d.domain.testdb.Employee;
import com.anhe3d.repository.testdb.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.util.Collection;


@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.2.114", "http://anhe-internal.local"})
@RestController
@Transactional(
        propagation = Propagation.SUPPORTS,
        readOnly = true
)
public class EmployeeController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    EmployeeRepository employeeRepository;



//    @Cacheable(
//            value = "employees",
//            key = "#id")
//    @CachePut(
//            value = "employees",
//            key = "#result.id")
//    @RequestMapping(
//            path = "/api/employees/{id}",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Employee> getEmployeeById(@PathVariable("id") Long id) {
//        Employee employee = employeeService.findOne(id);
//        if (employee == null) {
//            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity<>(employee, HttpStatus.ACCEPTED);
//    }

    //Browser will additionally contain http options. Below which response that request
    @RequestMapping(method = RequestMethod.OPTIONS)
    public ResponseEntity options(HttpServletResponse response) {
        response.setHeader("Allow", "HEAD,GET,POST,PUT,OPTIONS");
        return new ResponseEntity(HttpStatus.OK);
    }

//    @RequestMapping(value = "/api/employees", method = RequestMethod.OPTIONS)
//    public ResponseEntity options(HttpServletResponse response) {
//        response.setHeader("Allow", "HEAD,GET,PUT,OPTIONS");
//        return new ResponseEntity(HttpStatus.OK);
//    }

//利用employeeId來查詢employee資料
/*
 * 取得某員工資料
 * @param GET 從資料庫取得資料
 * @param produces JSON型態
 * @param {employeeId} 網址輸入想查詢的員工ID
 * @param @PathVariable 讀取網址中的"employeeId"
 * @param employeeId 型別為String
 * 若查無此員工，回傳NOT_FOUND
 * @return employee
 */
    @RequestMapping(
            path = "/api/employees/{employeeId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> getEmployeeByEmployeeId(@PathVariable("employeeId") String employeeId) {
        Employee employee = employeeRepository.findByEmployeeId(employeeId);
        if (employee == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
        logger.info("getEmployeeByEmployeeId");
        return new ResponseEntity<>(employee, HttpStatus.ACCEPTED);
    }

/*
 * 查詢所有employee資料
 * @param GET 從資料庫取得資料
 * @param produces JSON型態
 * @return Collection<Employee> employees
 */
    @RequestMapping(
            path = "/api/employees",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Collection<Employee>> getEmployees() {
        Collection<Employee> employees = (Collection<Employee>) employeeRepository.findAll();
        logger.info("getEmployees");
        return new ResponseEntity<>(employees, HttpStatus.OK);
    }
/*
 * 新增員工資料
 * @param POST 新增資料到資料庫
 * @param consumes 讓資料寫入，JSON型態
 * @param produces JSON型態
 * @param @RequestBody 接整個object
 * @param employee 要寫入的employee
 * @return 儲存寫入的資料
 */
    @RequestMapping(
            path = "/api/employees",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> createEmployee(@RequestBody Employee employee) {
        Employee savedEmployee = employeeRepository.save(employee);
        logger.info("createEmployee");
        return new ResponseEntity<>(savedEmployee, HttpStatus.CREATED);
    }

/*
 * 修改員工資料
 * @param PUT 新增資料到資料庫
 * @param consumes 讓資料寫入，JSON型態
 * @param produces JSON型態
 * @param @RequestBody 接整個object
 * @param {employeeId} 在網址打入要修改的員工ID
 * @param @PathVariable 讀取網址中的employeeId，並將它設為String型別
 * @param @RequestBody 接整個object
 * @param employee 要寫入的employee
 * @return 儲存寫入的資料
 */

    @RequestMapping(
//            path = "/api/employees",
            path = "/api/employees/{employeeId}",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> updateEmployee(
            @PathVariable("employeeId") String employeeId,
//            @RequestBody Map<String, String> changedParams
            @RequestBody Employee employee
    ) {
        Employee oldEmployee = employeeRepository.findByEmployeeId(employeeId);

        oldEmployee.updateField(employee);
        employeeRepository.save(oldEmployee);

        logger.info("updateEmployee");
        return new ResponseEntity<>(oldEmployee, HttpStatus.OK);
    }

    /*
     * 刪除員工資料
     * @param DELETE 刪除資料
     * @param {employeeId} 網址輸入想刪除員工ID
     * @param @PathVariable 讀取網址中的"employeeId"
     * 先查詢是否有此id的資料
     * 若無此資料回傳INTERNAL_SERVER_ERROR
     * 若有此資料 就刪除
     * @return NO_CONTENT
     */
    @RequestMapping(
            path = "/api/employees/{employeeId}",
            method = RequestMethod.DELETE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<Employee> deleteEmployee(
            @PathVariable("employeeId") String employeeId
    ) {
        Employee targetEmployee = employeeRepository.findByEmployeeId(employeeId);
        if (targetEmployee == null) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
        logger.info("deleteEmployee");
        employeeRepository.delete(targetEmployee);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }










//    @RequestMapping(
//            path = "/api/greetings",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Collection<Greeting2>> getGreetings() {
//        Collection<Greeting2> greeting2s = greetingMap.values();
//        return new ResponseEntity<>(greeting2s, HttpStatus.OK);
//    }
//
//    @Transactional(
//            propagation = Propagation.REQUIRED,
//            readOnly = false
//    )
//    @RequestMapping(
//            path = "/api/greetings",
//            method = RequestMethod.POST,
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Greeting2> createGreeting(@RequestBody Greeting2 greeting2) {
//        Greeting2 savedGreeting = save(greeting2);
//        if (savedGreeting.getId() == 4L) {
//            throw new RuntimeException("Roll me back");
//        }
//        return new ResponseEntity<>(savedGreeting, HttpStatus.CREATED);
//    }
//
//    @RequestMapping(
//            path = "/api/greetings/{Id}",
//            method = RequestMethod.PUT,
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<Greeting2> updateGreeting(@RequestBody Greeting2 greeting2) {
//        Greeting2 updatedGreeting = save(greeting2);
//        if (updatedGreeting == null) {
//            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
//        }
//
//        return new ResponseEntity<>(updatedGreeting, HttpStatus.OK);
//    }












}
