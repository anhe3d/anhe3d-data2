package com.anhe3d.web.api;

import com.anhe3d.domain.testdb.Shop;
import com.anhe3d.domain.testdb.ShopMenu;
import com.anhe3d.domain.testdb.ShopOrder;
import com.anhe3d.repository.testdb.EmployeeRepository;
import com.anhe3d.repository.testdb.ShopMenuRepository;
import com.anhe3d.repository.testdb.ShopOrderRepository;
import com.anhe3d.repository.testdb.ShopRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.util.Collection;
@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.2.114", "http://anhe-internal.local"})
@RestController
@RequestMapping(path = "/api/meal-order")
public class MealOrderController {

    private static final Logger logger = LoggerFactory.getLogger(EmployeeController.class);

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private ShopMenuRepository shopMenuRepository;

    @Autowired
    private ShopOrderRepository shopOrderRepository;

    @Autowired
    private EmployeeRepository employeeRepository;

    //Get all shops
    @RequestMapping(
            path = "/shops",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getShops() {
        Collection<Shop> shops = (Collection<Shop>) shopRepository.findAll();
        logger.info("getShops");
        return new ResponseEntity<>(shops, HttpStatus.OK);
    }

    //Get all shopMenu by shopId
    @RequestMapping(
            path = "/menus/{shopName}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getMenus(@PathVariable("shopName") String shopName) {
        Collection<ShopMenu> shopMenus = (Collection<ShopMenu>) shopMenuRepository.findAllByShop(shopRepository.findByShopName(shopName));
        logger.info("getMenus");
        return new ResponseEntity<>(shopMenus, HttpStatus.OK);
    }

    //Get all shopOrders should be mealOrder...
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getShopOrders() {
        Collection<ShopOrder> mealOrders = (Collection<ShopOrder>) shopOrderRepository.findAll();
        logger.info("getMealOrders");
        return new ResponseEntity<>(mealOrders, HttpStatus.OK);
    }

    ///Create new shopOrder
    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<ShopOrder> postShopOrder(
            @RequestBody ShopOrder shopOrder
//            @RequestBody HashMap<String, String> requests
    ) {
        shopOrder.setEmployee(employeeRepository.findByEmployeeId(shopOrder.getEmployee().getEmployeeId()));
        java.util.Date Now = new java.util.Date();
        shopOrder.setOrderTime(new Timestamp(Now.getTime()));

//        ShopOrder shopOrder = new ShopOrder(
//                employeeRepository.findOne(Long.parseLong(requests.get("employee"))),
//                shopMenuRepository.findOne(Long.parseLong(requests.get("shopMenu"))),
//                new Timestamp(Now.getTime()),
//                Integer.parseInt(requests.get("quantity")),
//                Double.parseDouble(requests.get("totalPrice")),
//                requests.get("remark")
//        );

        logger.info(shopOrder.toString());
        shopOrderRepository.save(shopOrder);
        logger.info("postShopOrder");
        return new ResponseEntity<>(shopOrder, HttpStatus.CREATED);
    }


}
