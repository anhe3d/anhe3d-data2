package com.anhe3d.web.api;


import com.anhe3d.domain.crmdb.Distributor;
import com.anhe3d.domain.salesdb.SalesItem;
import com.anhe3d.domain.salesdb.SalesOrder;
import com.anhe3d.domain.salesdb.SoItem;
import com.anhe3d.domain.salesdb.SoStatus;

import com.anhe3d.repository.crmdb.DistributorRepository;
import com.anhe3d.repository.salesdb.SalesItemRepository;
import com.anhe3d.repository.salesdb.SalesOrderRepository;
import com.anhe3d.repository.salesdb.SoItemRepository;
import com.anhe3d.repository.salesdb.SoStatusRepository;
import com.anhe3d.repository.stocksdb.BomRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.ParseException;
import java.util.*;


@CrossOrigin(origins = {"http://localhost:3000", "http://192.168.2.114", "http://anhe-internal.local"})
@RestController
@RequestMapping(path = "/api/sales-order")
public class SalesOrderController {
    private static final Logger logger = LoggerFactory.getLogger(SalesOrderController.class);
    @Autowired
    private DistributorRepository distributorRepository;
    @Autowired
    private SalesItemRepository salesItemRepository;
    @Autowired
    private SalesOrderRepository salesOrderRepository;
    @Autowired
    private SoItemRepository soItemRepository;
    @Autowired
    private SoStatusRepository soStatusRepository;
    @Autowired
    private BomRepository bomRepository;

    @RequestMapping(
            path = "/distributors",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getDistributors() {
        Collection<Distributor> distributors = (Collection<Distributor>) distributorRepository.findAll();
        logger.info("getDistributors");
        return new ResponseEntity<>(distributors, HttpStatus.OK);
    }

//    @RequestMapping(
//            path = "/sales-item/{distributor}",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity getSalesItemByDistributor(@PathVariable("distributor") String distributor) {
//        Distributor distributor1 = distributorRepository.findByDtName(distributor);
//        Collection<SalesItem> salesItems = (Collection<SalesItem>) salesItemRepository.findAllByDistributor(distributor1);
//        return new ResponseEntity<>(salesItems, HttpStatus.OK);
//    }

    @RequestMapping(
            path = "/so-item/{soId}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getSoItemBySoId(@PathVariable("soId") Long soId){
        SalesOrder salesOrder = salesOrderRepository.findOne(soId);
        System.out.println(salesOrder);
        Iterable<SoItem> soItems = soItemRepository.findAllBySalesOrder(salesOrder);
        logger.info("getAllSoItem");
        return new ResponseEntity<>(soItems, HttpStatus.OK);
    }
    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllSalesOrder() {
        Collection<SalesOrder> salesOrders = (Collection<SalesOrder>) salesOrderRepository.findAll();
        logger.info("getAllSalesOrder");
        return new ResponseEntity<>(salesOrders, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/sales-item",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllSalesItem() {
        Collection<SalesItem> salesItems = (Collection<SalesItem>) salesItemRepository.findAll();
        logger.info("getAllSalesItem");
        return new ResponseEntity<>(salesItems, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/sales-item/{sitemid}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllSalesItem(@PathVariable("sitemid") Long sitemId) {
        SalesItem salesItem = salesItemRepository.findOne(sitemId);
//        Collection<SalesItem> salesItems = (Collection<SalesItem>) salesItemRepository.findAll();
        logger.info("getAllSalesItem");
        return new ResponseEntity<>(salesItem, HttpStatus.OK);
    }

    @RequestMapping(
            path = "/so-item",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity getAllSoItem() {
        Collection<SoItem> soItems = (Collection<SoItem>) soItemRepository.findAll();
        logger.info("getAllSoItem");
        return new ResponseEntity<>(soItems, HttpStatus.OK);
    }


    @RequestMapping(
            path = "/so-status",
            method = RequestMethod.PUT,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SoStatus> updateSoStatus(@RequestBody SoStatus soStatus) {
        SoStatus newSoStatus = soStatusRepository.save(soStatus);
        logger.info("updateSoStatus");
        return new ResponseEntity<>(newSoStatus, HttpStatus.OK);
    }

//    @RequestMapping(
//            path = "/so-item",
//            method = RequestMethod.POST,
//            consumes = MediaType.APPLICATION_JSON_VALUE,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity<SoItem> createSoItem(@RequestBody SoItem soItem) {
//        soItemRepository.save(soItem);
//        logger.info("createSoItem");
//        return new ResponseEntity<>(soItem, HttpStatus.CREATED);
//    }

    @RequestMapping(
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SalesOrder> createSalesOrderAll(@RequestBody String json) throws IOException {
        logger.info("createSalesOrderAll");

        ObjectMapper mapper = new ObjectMapper();
        JSONObject obj = new JSONObject(json);

        SalesOrder salesOrder = mapper.readValue(obj.getJSONObject("salesOrder").toString(), SalesOrder.class);
        System.out.println(salesOrder);
        Timestamp timestamp = new Timestamp(new Date().getTime());
        SoStatus soStatus = salesOrder.getSoStatus();
        soStatus.setUpdateTime(timestamp);
        soStatusRepository.save(soStatus);
        salesOrder.setCreateTime(timestamp);
        salesOrder.setSoStatus(soStatusRepository.findOne(soStatus.getStatusId()));
        SalesOrder savedSalesOrder = salesOrderRepository.save(salesOrder);


        JSONArray soItems = obj.getJSONArray("soItems");
        System.out.println(soItems.length());
        for (int i = 0; i < soItems.length(); i++) {
            SoItem soItem = mapper.readValue(soItems.getJSONObject(i).toString(), SoItem.class);
            System.out.println(soItem);
            soItem.setSalesOrder(savedSalesOrder);
            soItemRepository.save(soItem);
        }

        return new ResponseEntity<SalesOrder>(savedSalesOrder,HttpStatus.CREATED);

    }

//    @RequestMapping(
//            path = "/sales-bom",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity getAllSalesBom() {
//        Collection<SalesBOM> salesBOMs = (Collection<SalesBOM>) salesBOMRepository.findAll();
//        logger.info("getSalesBOMs");
//        return new ResponseEntity<>(salesBOMs, HttpStatus.OK);
//    }


//    get sales BOM
//
//    @RequestMapping(
//            path = "/sales-bom/{salesitem}",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity getSalesBom(@PathVariable("salesitem") String salesItem) {
//        SalesItem salesItem1 = salesItemRepository.findBySitemName(salesItem);
//        Collection<SalesBOM> salesBOMs = (Collection<SalesBOM>) salesBOMRepository.findAllBySalesItem(salesItem1);
////        List<SemiMaterialBOM> semiMaterialBOMs = new ArrayList<>();
////        List<Long> sbId = new ArrayList<>();
////        salesBOMRepository.findAllBySalesItem(salesItem1).forEach(salesBOMs::add);
////        for (int i=0;i<salesBOMs.size();i++){
//////            SemiMaterialBOM semiMaterialBOM = semiMaterialBOMRepository.findBySalesBOM(salesBOMs.get(i));
////            Long semiMaterialBOM = salesBOMs.get(i).getSbId();
////            sbId.add(semiMaterialBOM);
////        }
////        Collection<SalesBOM> salesBOMs = (Collection<SalesBOM>) salesBOMRepository.findAllBySalesItem(salesItemRepository.findBySitemName(salesItem));
//        logger.info("getSalesBOMs");
//        return new ResponseEntity<>(salesBOMs, HttpStatus.OK);
//    }
//
//
//    @RequestMapping(
//            path = "/semi-material-bom/{sbId}",
//            method = RequestMethod.GET,
//            produces = MediaType.APPLICATION_JSON_VALUE)
//    public ResponseEntity getSemiMaterialBOM(@PathVariable("sbId") Long sbId) {
//        Collection<SemiMaterialBOM> semiMaterialBOMs = (Collection<SemiMaterialBOM>) semiMaterialBOMRepository.findAllBySalesBOM(salesBOMRepository.findOne(sbId));
////        List<SemiMaterialBOM> semiMaterialBOMs = new ArrayList<>();
////        List<Item> items = new ArrayList<>();
////        semiMaterialBOMRepository.findAllBySalesBOM(salesBOMRepository.findOne(sbId)).forEach(semiMaterialBOMs::add);
////        for (int i=0;i<semiMaterialBOMs.size();i++){
////            Item item = semiMaterialBOMs.get(i).getItem();
////            items.add(item);
////        }
////        Collection<SemiMaterialBOM> semiMaterialBOMs = (Collection<SemiMaterialBOM>) semiMaterialBOMRepository.findAllBySalesBOM(salesBOMRepository.findOne(sbId));
//        logger.info("getSemiMaterialBOMs");
//        return new ResponseEntity<>(semiMaterialBOMs, HttpStatus.OK);
//    }


}
