import com.anhe3d.Application;
import com.anhe3d.domain.crmdb.Distributor;
import com.anhe3d.domain.salesdb.*;
import com.anhe3d.repository.crmdb.DistributorRepository;
import com.anhe3d.repository.salesdb.*;
import com.anhe3d.repository.stocksdb.BomRepository;
import com.anhe3d.repository.stocksdb.ItemTypeRepository;
import com.anhe3d.repository.testdb.EmployeeRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class SalesDBTest {

    @Autowired
    private SalesOrderRepository salesOrderRepository;
    @Autowired
    private SoItemRepository soItemRepository;
    @Autowired
    private SoStatusRepository soStatusRepository;
    @Autowired
    private DistributorRepository distributorRepository;
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private SalesItemRepository salesItemRepository;
    @Autowired
    private BomRepository bomRepository;
    @Autowired
    private ItemTypeRepository itemTypeRepository;


    @Test
    public void testGetAllSalesItem() {
        List<SalesItem> salesItems = new ArrayList<>();
        salesItemRepository.findAll().forEach(salesItems::add);
        System.out.println(salesItems);
        Assert.assertNotNull(salesItems);
    }


    @Test
    public void testDeleteSalesOrder() {
        SalesOrder salesOrder = salesOrderRepository.findByInvoiceNo("11");
        salesOrderRepository.delete(salesOrder);
    }

    @Test
    public void testPostSalesOrder(){
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        Timestamp timestamp1 = Timestamp.valueOf("2016-11-10 13:00:00");
        Distributor distributor = distributorRepository.findOne(1L);
        SalesOrder salesOrder =new SalesOrder(distributor,soStatusRepository.findOne(1L),0.0,0.0,timestamp,employeeRepository.findOne(1L),timestamp1,timestamp1,"11",0.0,0.0);
        salesOrderRepository.save(salesOrder);
        System.out.println(salesOrder);
    }
    @Test
    public void testGetAllSalesOrder() {
        List<SalesOrder> salesOrders = new ArrayList<>();
        salesOrderRepository.findAll().forEach(salesOrders::add);
        salesOrders.forEach(salesOrder -> System.out.println(salesOrder.toString()));
        Assert.assertNotNull(salesOrders);
    }

//    @Test
//    public void testPostSoItem() {
//        SoItem soItem = new SoItem(salesOrderRepository.findByInvoiceNo("22"),salesItemRepository.findOne(1L),1,130000.0);
//        soItemRepository.save(soItem);
//        System.out.println(soItem);
//    }



    @Test
    public void testGetAllSoItem() {
        List<SoItem> soItems = new ArrayList<>();
        soItemRepository.findAll().forEach(soItems::add);
        soItems.forEach(soItem -> System.out.println(soItem.toString()));
        Assert.assertNotNull(soItems);
    }

    @Test
    public void testUpdateSoStatus() {
        SoStatus soStatus = soStatusRepository.findOne(1L);
        soStatus.setCreated(false);
        soStatusRepository.save(soStatus);
        System.out.println(soStatus);
        System.out.println("testUpdate");
    }


    @Test
    public void testGetAllSoStatus() {
        List<SoStatus> soStatuses = new ArrayList<>();
        soStatusRepository.findAll().forEach(soStatuses::add);
        soStatuses.forEach(soStatus -> System.out.println(soStatus.toString()));
        Assert.assertNotNull(soStatuses);
    }

    @Test
    public void testPostSoStatus() {
        SoStatus soStatus = new SoStatus(Timestamp.valueOf(LocalDateTime.now()),false,false,false,false,false,false,false,false);
        soStatusRepository.save(soStatus);
        System.out.println(soStatus);
    }

    @Test
    public void testGetSalesItem() {
        SalesItem salesItem = salesItemRepository.findOne(1L);
        System.out.println(salesItem);
    }
    @Test
    public void testPostSalesItem() {
        SalesItem salesItem = new SalesItem("123",130000.0,itemTypeRepository.findOne(3L),"123",Timestamp.valueOf(LocalDateTime.now()),Timestamp.valueOf(LocalDateTime.now()),false,bomRepository.findOne(1L));
        salesItemRepository.save(salesItem);
        System.out.println(salesItem);

    }

    @Test
    public void testGetSoItem() {
        SoItem soItem = soItemRepository.findOne(1L);
        System.out.println(soItem);
    }
    @Test
    public void testGetAllBySalesOrder() {
        SalesOrder salesOrder = salesOrderRepository.findOne(1L);
        Iterable<SoItem> soItems = soItemRepository.findAllBySalesOrder(salesOrder);
        System.out.println(soItems);
    }
}
