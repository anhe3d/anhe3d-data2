import com.anhe3d.Application;
import com.anhe3d.domain.crmdb.Vendor;
import com.anhe3d.domain.vendordb.*;
import com.anhe3d.repository.crmdb.VendorRepository;
import com.anhe3d.repository.stocksdb.ItemRepository;
import com.anhe3d.repository.testdb.EmployeeRepository;
import com.anhe3d.repository.vendordb.*;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class VendorDBTest {
    @Autowired
    private EmployeeRepository employeeRepository;
    @Autowired
    private VendorRepository vendorRepository;

    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private ItemTestRepository itemTestRepository;



    @Test
    public void testGetAllVendor() {
        List<Vendor> vendors = new ArrayList<>();
        vendorRepository.findAll().forEach(vendors::add);
        System.out.println("testFindAll");
        vendors.forEach(vendor -> System.out.println(vendor.toString()));
        Assert.assertNotNull(vendors);
    }


    @Test
    public void testGetAllVendors(){
        List<Vendor> vendors = new ArrayList<>();
        vendorRepository.findAll().forEach(vendors::add);
        System.out.println(vendors);
    }

    @Test
    public void testGetAllItemTest(){
        Vendor vendor = vendorRepository.findOne(1L);
        Collection<ItemTest> itemTests = (Collection<ItemTest>) itemTestRepository.findAllByVendor(vendor);
        System.out.println(itemTests);
    }
    @Test
    public void testGetAllItemTest1() throws IOException{
        ItemTest itemTest = itemTestRepository.findOne(1L);

        FileReader fr = new FileReader("C:\\Logos.jpg");
        BufferedReader br = new BufferedReader(fr);
        while (br.ready()) {
            System.out.println(br.readLine());
        }
        fr.close();


        //        File file1 = new File("C:\\Logos.jpg");
//        itemTest.setImage(file1);
//        System.out.println(itemTest);
    }


}