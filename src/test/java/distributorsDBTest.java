import com.anhe3d.Application;
import com.anhe3d.domain.crmdb.Distributor;
import com.anhe3d.repository.crmdb.DistributorRepository;
import com.anhe3d.repository.salesdb.SalesItemRepository;
import com.anhe3d.repository.stocksdb.ItemTypeRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class DistributorsDBTest {
    @Autowired
    private DistributorRepository distributorRepository;
    @Autowired
    private SalesItemRepository salesItemRepository;
    @Autowired
    ItemTypeRepository itemTypeRepository;

    @Test
    public void testGetAllDistributor() {
        List<Distributor> distributors = new ArrayList<>();
        distributorRepository.findAll().forEach(distributors::add);
        distributors.forEach(distributor -> System.out.println(distributor.getDtName()));
        Assert.assertNotNull(distributors);
    }

    @Test
    public void testPostDistributor() {
        Distributor distributor = new Distributor("123","12345678");
        distributorRepository.save(distributor);
        System.out.println(distributor);
    }


//    @Test
//    public void testPostSalesItem(){
//        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
//        SalesItem salesItem = new SalesItem("test",distributorRepository.findOne(2L),1000.0,itemTypeRepository.findOne(3L),"123",timestamp,timestamp,false);
//        salesItemRepository.save(salesItem);
//    }


}
