import com.anhe3d.Application;
import com.anhe3d.domain.stocksdb.*;

import com.anhe3d.repository.purchasedb.PurchaseOrderRepository;
import com.anhe3d.repository.stocksdb.*;

import com.anhe3d.repository.crmdb.VendorRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class StocksDBTest {

    @Autowired
    private VendorRepository vendorRepository;
    @Autowired
    private PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private BatchRepository batchRepository;
    @Autowired
    private StockRepository stockRepository;
    @Autowired
    private ItemCategoryRepository itemCategoryRepository;
    @Autowired
    private ItemUnitRepository itemUnitRepository;

//    @Test
//    public void testDeleteSalesOrder() {
//        SalesOrder salesOrder = salesOrderRepository.findByInvoiceNo("33");
//        salesOrderRepository.delete(salesOrder);
//    }
//
//    @Test
//    public void testPostSalesOrder(){
//        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
//        Timestamp timestamp1 = Timestamp.valueOf("2016-11-10 13:00:00");
//        Distributor distributor = distributorRepository.findOne(1L);
//        SalesOrder salesOrder =new SalesOrder(distributor,soStatusRepository.findOne(1L),0.0,0.0,timestamp,employeeRepository.findOne(1L),timestamp1,timestamp1,"11",0.0,0.0);
//        salesOrderRepository.save(salesOrder);
//        System.out.println(salesOrder);
//    }

//    @Test
//    public void testPostSoItem() {
//        SoItem soItem = new SoItem(salesOrderRepository.findByInvoiceNo("22"),salesItemRepository.findOne(1L),1,130000.0);
//        soItemRepository.save(soItem);
//        System.out.println(soItem);
//    }
//


//    @Test
//    public void testGetAllSoItem() {
//        List<StoragePlace> storagePlaces = new ArrayList<>();
//        storagePlaceRepository.findAll().forEach(storagePlaces::add);
//        storagePlaces.forEach(storagePlace -> System.out.println(storagePlace.toString()));
//        Assert.assertNotNull(storagePlaces);
//    }

//    @Test
//    public void testUpdateSoStatus() {
//        SoStatus soStatus = soStatusRepository.findOne(1L);
//        soStatus.setCreated(true);
//        soStatusRepository.save(soStatus);
//        System.out.println(soStatus);
//        System.out.println("testUpdate");
//    }
//

    @Test
    public void testGetAllBatch() {
        List<Batch> batches = new ArrayList<>();
//        batchRepository.findAll().forEach(batch -> {batch});
        batchRepository.findAll().forEach(batches::add);
        batches.forEach(batch -> System.out.println(batch.toString()));
        Assert.assertNotNull(batches);
    }

    @Test
    public void testPostBatch() {
        Batch batch = new Batch(purchaseOrderRepository.findOne(1L),itemRepository.findOne(2L),4);
        batchRepository.save(batch);
        System.out.println(batch);
    }

    @Test
    public void testGetAllStock() {
        List<Stock> stocks = new ArrayList<>();
        stockRepository.findAll().forEach(stocks::add);
        stocks.forEach(stock -> System.out.println(stock.toString()));
        Assert.assertNotNull(stocks);
    }
//    @Test
//    public void testPostStock() {
//        Stock stock = new Stock(13L,itemRepository.findOne(13L),warehouseRepository.findOne(1L),10,15);
//        stockRepository.save(stock);
//        System.out.println(stock);
//    }

    @Test
    public void testUpdateStock() {
        Stock stock = stockRepository.findOne(1L);
        stock.setQuantity(9);
        stockRepository.save(stock);
        System.out.println(stock);
    }

    @Test
    public void testGetAllItem() {
        List<Item> items = new ArrayList<>();
        itemRepository.findAll().forEach(items::add);
        System.out.println(items);
    }
    @Test
    public void testGetAllItemUnit() {
//        ItemUnit itemUnit = new ItemUnit("片" );
//        itemUnitRepository.save(itemUnit);
//        System.out.println(itemUnitRepository.findOne(5L));
        List<ItemUnit> itemUnits = new ArrayList<>();
        itemUnitRepository.findAll().forEach(itemUnits::add);
        System.out.println(itemUnits);
    }
    @Test
    public void testGetAllItemCategory() {
        List<ItemCategory> itemCategories = new ArrayList<>();
        itemCategoryRepository.findAll().forEach(itemCategories::add);
        System.out.println(itemCategories);
    }


}
