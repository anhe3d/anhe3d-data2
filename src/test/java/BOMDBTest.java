import com.anhe3d.Application;
import com.anhe3d.domain.stocksdb.Bom;
import com.anhe3d.repository.stocksdb.BomRepository;
import com.anhe3d.repository.salesdb.SalesItemRepository;
import com.anhe3d.repository.stocksdb.ItemRepository;
import com.anhe3d.repository.crmdb.VendorRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.ArrayList;
import java.util.List;


@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class BOMDBTest {


    @Autowired
    private SalesItemRepository salesItemRepository;
    @Autowired
    private VendorRepository vendorRepository;
    @Autowired
    private ItemRepository itemRepository;
    @Autowired
    private BomRepository bomRepository;

    @Test
    public void testPostBOM() {
        Bom bom = new Bom("DASINK",null,itemRepository.findOne(13L),1);
        bomRepository.save(bom);
        System.out.println(bom);
    }
//
//    @Test
//    public void testDeletesemiMaterialBOM() {
//        SemiMaterialBOM semiMaterialBOM = semiMaterialBOMRepository.findOne(1L);
//        semiMaterialBOMRepository.delete(semiMaterialBOM);
//    }
//
//    @Test
//    public void testPostSemiMaterialBOM(){
//        SemiMaterialBOM semiMaterialBOM = new SemiMaterialBOM("123",itemRepository.findOne(13L),salesBOMRepository.findOne(1L));
//        semiMaterialBOMRepository.save(semiMaterialBOM);
//        System.out.println(semiMaterialBOM);
//    }
//    @Test
//    public void testGetAllSalesOrder() {
//        List<SalesOrder> salesOrders = new ArrayList<>();
//        salesOrderRepository.findAll().forEach(salesOrders::add);
//        salesOrders.forEach(salesOrder -> System.out.println(salesOrder.toString()));
//        Assert.assertNotNull(salesOrders);
//    }





    @Test
    public void testGetAllBom() {
        List<Bom> bomList = new ArrayList<>();
        bomRepository.findAll().forEach(bomList::add);
        bomList.forEach(bom -> System.out.println(bom.toString()));
    }

    @Test
    public void findAllByBomName() {
        Bom bom = bomRepository.findByBomName("DASINK");
        Iterable<Bom> boms = bomRepository.findAllByBom(bom);
         System.out.println(boms);
    }
//    @Test
//    public void testUpdateSoStatus() {
//        SoStatus soStatus = soStatusRepository.findOne(1L);
//        soStatus.setCreated(true);
//        soStatusRepository.save(soStatus);
//        System.out.println(soStatus);
//        System.out.println("testUpdate");
//    }
//
//
//    @Test
//    public void testGetAllSoStatus() {
//        List<SoStatus> soStatuses = new ArrayList<>();
//        soStatusRepository.findAll().forEach(soStatuses::add);
//        soStatuses.forEach(soStatus -> System.out.println(soStatus.toString()));
//        Assert.assertNotNull(soStatuses);
//    }
//
//    @Test
//    public void testPostSoStatus() {
//        SoStatus soStatus = new SoStatus(Timestamp.valueOf(LocalDateTime.now()),false,false,false,false,false,false,false,false);
//        soStatusRepository.save(soStatus);
//        System.out.println(soStatus);
//    }






}
