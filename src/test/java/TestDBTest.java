import com.anhe3d.Application;
import com.anhe3d.domain.testdb.Employee;
import com.anhe3d.domain.testdb.Shop;
import com.anhe3d.domain.testdb.ShopMenu;
import com.anhe3d.domain.testdb.ShopOrder;
import com.anhe3d.repository.testdb.EmployeeRepository;
import com.anhe3d.repository.testdb.ShopMenuRepository;
import com.anhe3d.repository.testdb.ShopOrderRepository;
import com.anhe3d.repository.testdb.ShopRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kevinhung on 6/8/16.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class TestDBTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private ShopRepository shopRepository;

    @Autowired
    private ShopMenuRepository shopMenuRepository;

    @Autowired
    private ShopOrderRepository shopOrderRepository;


    @Test
    public void testGetAllEmployee() {
        List<Employee> employees = new ArrayList<>();
        employeeRepository.findAll().forEach(employees::add);
        System.out.println("testGetAllEmployee");
        employees.forEach(employee -> System.out.println(employee.getLastName()));
        Assert.assertNotNull(employees);
    }

    @Test
    public void testPostEmployee() {
        Employee employee = new Employee("9999", "建輝", "吳", "0936049703");
        employeeRepository.save(employee);
        System.out.println("testPostEmployee");

    }

    @Test
    public void testUpdateEmployee() {
        Employee employee = employeeRepository.findByEmployeeId("9999");
        employee.setFirstName("建輝大大");
        employeeRepository.save(employee);
        System.out.println("testUpdateEmployee");

    }

    @Test
    public void testDeleteEmployee() {
        Employee employee = employeeRepository.findByEmployeeId("9999");
        employeeRepository.delete(employee);
    }

    @Test
    public void testGetMenu() {
        ShopMenu shopMenu = shopMenuRepository.findOne(1L);
        System.out.println(shopMenu.toString());
        List<ShopMenu> shopMenus = new ArrayList<>();
        shopMenuRepository.findAll().forEach(shopMenus::add);
        System.out.println(shopMenus);
    }
    @Test
    public void testGetShop() {
//        System.out.println(shopRepository.findOne(1L));
        List<Shop> shops = new ArrayList<>();
        shopRepository.findAll().forEach(shops::add);
        System.out.println(shops);
    }
    @Test
    public void testGetShopOrder(){
        List<ShopOrder> shopOrders= new ArrayList<>();
        shopOrderRepository.findAll().forEach(shopOrders::add);
        System.out.println(shopOrders);
//        ShopOrder shopOrder = shopOrderRepository.findOne(1L);
//        System.out.println(shopOrder.toString());
    }

    @Test
    public void testPostShopOrder(){
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
        ShopOrder shopOrder = new ShopOrder(employeeRepository.findOne(1L),shopMenuRepository.findOne(1L), timestamp ,1,1*shopMenuRepository.findOne(1L).getItemPrice(),"");
        shopOrderRepository.save(shopOrder);
    }
    //TODO ShopGet, ShopMenuGet, ShopOrderGet, ShopOrderPost


}
