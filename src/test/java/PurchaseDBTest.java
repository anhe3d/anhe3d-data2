import com.anhe3d.Application;
import com.anhe3d.domain.purchasedb.PoItem;
import com.anhe3d.domain.purchasedb.PoStatus;
import com.anhe3d.domain.purchasedb.PurchaseOrder;
import com.anhe3d.repository.purchasedb.PoItemRepository;
import com.anhe3d.repository.purchasedb.PoStatusRepository;
import com.anhe3d.repository.purchasedb.PurchaseOrderRepository;
import com.anhe3d.repository.testdb.EmployeeRepository;
import com.anhe3d.repository.crmdb.VendorRepository;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by USER on 2016/6/8.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(Application.class)
public class PurchaseDBTest {
    @Autowired
    PurchaseOrderRepository purchaseOrderRepository;
    @Autowired
    PoStatusRepository poStatusRepository;
    @Autowired
    PoItemRepository poItemRepository;
    @Autowired
    VendorRepository vendorRepository;
    @Autowired
    EmployeeRepository employeeRepository;


    @Test
    public void testGetAllPurchaseOrder() {
        List<PurchaseOrder> pos = new ArrayList<>();
        purchaseOrderRepository.findAll().forEach(pos::add);
        System.out.println("testGetAllPurchaseOrder");
        pos.forEach(po -> System.out.println(po.getEmployee().getFirstName()));
        Assert.assertNotNull(pos);
    }
    @Test
    public void testPurchaseOrder() {
        String a ="2016-06-08 13:28:40";
        Timestamp timestamp = Timestamp.valueOf(a);
        PurchaseOrder purchaseOrder = purchaseOrderRepository.findByCreateTime(timestamp);
        System.out.println(purchaseOrder.toString());
    }

    @Test
    public void testPostPurchaseOrderAndPoItem() {

    }
//    @Test
//    public void testGetAllPoItemByPoId() {
////        PoItem poItem = poItemRepository.findByPurchaseOrder(purchaseOrderRepository.findOne(1L));
////        System.out.println(poItem);
////        Collection<PoItem> allPoItem =( Collection<PoItem>) poItemRepository.findAllByPurchaseOrder(purchaseOrderRepository.findOne(1L));
//    Iterable<PurchaseOrder> poItems = poItemRepository.findAllByPurchaseOrder(purchaseOrderRepository.findOne(1L));
//        System.out.println(poItems);
//    }

    @Test
    public void testGetAllPoItem() {
        List<PoItem> poItems = new ArrayList<>();
        poItemRepository.findAll().forEach(poItems::add);
        System.out.println(poItems);
    }
//    @Test
//    public void testGetPoItemByPrice() {
//        PoItem poItem = poItemRepository.findByPrice(800.0);
//        System.out.println(poItem);
//    }
    @Test
    public void testGetAllPoItemByPrice() {

//        Collection<Double> allPoItem = (Collection<Double>) poItemRepository.findAllByPrice(800.0);
//        System.out.println(allPoItem);
    }

    @Test
    public void testGetPoStatusByPoId() {
        PoStatus poStatus = poStatusRepository.findOne(purchaseOrderRepository.findOne(2L).getPoStatus().getStatusId());
        System.out.println(poStatus);
    }
    @Test
    public void testPostPurchaseOrder() {
        Timestamp timestamp = Timestamp.valueOf(LocalDateTime.now());
//        Vendor vendor = vendorRepository.findOne(5L);
//        PoStatus poStatus = poStatusRepository.findOne(3L);
//        Employee employee = employeeRepository.findOne(4L);
//        PurchaseOrder purchaseOrder = new PurchaseOrder(vendor,poStatus,0.0,100.0,employee);
//        System.out.println(purchaseOrder.toString());
//        purchaseOrderRepository.save(purchaseOrder);
        System.out.println(timestamp.getTime());
    }
    @Test
    public void testDeletePurchaseOrder() {
        boolean deleted =purchaseOrderRepository.exists(9L);
        System.out.println(deleted);
        purchaseOrderRepository.delete(9L);
    }

}
